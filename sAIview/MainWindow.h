#pragma once

#include "..\COMMON\aipolicy.h"
#include "LoadingScreen.h"
#include "TranslatorService.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

/// <summary>
/// Summary for MainWindow
///
/// WARNING: If you change the name of this class, you will need to change the
///          'Resource File Name' property for the managed resource compiler tool
///          associated with all .resx files this class depends on.  Otherwise,
///          the designers will not be able to interact properly with localized
///          resources associated with this form.
/// </summary>
public ref class MainWindow : public System::Windows::Forms::Form
{
	public:
		MainWindow(void)
		{
			InitializeComponent();
			this->Icon = (Drawing::Icon^)((gcnew Resources::ResourceManager("sTools.icons", Reflection::Assembly::GetExecutingAssembly()))->GetObject("app"));

			Cursor = Windows::Forms::Cursors::WaitCursor;
			LoadingScreen^ initialize = gcnew LoadingScreen();
			initialize->Show();
			Application::DoEvents();

			skillReplace = loadSkillReplace("skill_replace.txt");
			blockDisable = loadBlockDisable("block_disable.txt");
			GUID = 1;

			try
			{
				soap = gcnew TranslatorService::SoapService();
				appID = "763743C8043FA427227700179E653860B731D443";
				from = "zh-CHS";
				to = "en";
				comboBox_language->Items->AddRange(soap->GetLanguagesForTranslate(appID));
				comboBox_language->SelectedItem = "en";
			}
			catch(Exception^ e)
			{
				soap = nullptr;
				textBox_translation->Text = "TRANSLATION SERVICE UNAVAILABLE";
				MessageBox::Show(e->Message);
			}
			initialize->Close();
			Cursor = Windows::Forms::Cursors::Default;
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MainWindow()
		{
			if (components)
			{
				delete components;
			}
		}

		TranslatorService::SoapService^ soap;
		String^ appID;
		String^ from;
		String^ to;
		Collections::SortedList^ skillReplace;
		ArrayList^ blockDisable;

		private: AIPolicy^ AI;
		private: int GUID;

		private: System::Windows::Forms::MenuStrip^  menuStrip_mainMenu;
		private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem1;
		private: System::Windows::Forms::ToolStripMenuItem^  loadToolStripMenuItem;
		private: System::Windows::Forms::ToolStripMenuItem^  saveToolStripMenuItem;
		private: System::Windows::Forms::ListBox^  listBox_ActionController;
		private: System::Windows::Forms::Label^  label2;
		private: System::Windows::Forms::ListBox^  listBox_ActionSet;
		private: System::Windows::Forms::Label^  label3;
		private: System::Windows::Forms::Label^  label4;
		private: System::Windows::Forms::ListBox^  listBox_Procedures;
		private: System::Windows::Forms::GroupBox^  groupBox1;
		private: System::Windows::Forms::GroupBox^  groupBox2;
		private: System::Windows::Forms::TextBox^  textBox_Condition;
		private: System::Windows::Forms::ComboBox^  comboBox_language;
		private: System::Windows::Forms::TextBox^  textBox_translation;
		private: System::Windows::Forms::Label^  label6;
		private: System::Windows::Forms::Button^  button_search;
		private: System::Windows::Forms::TextBox^  textBox_pattern;
		private: System::Windows::Forms::ComboBox^  comboBox_subCat;
		private: System::Windows::Forms::ComboBox^  comboBox_cat;
		private: System::Windows::Forms::Label^  label5;

		private: System::Windows::Forms::ToolStripMenuItem^  analyzeToolStripMenuItem;
private: System::Windows::Forms::ContextMenuStrip^  contextMenuStrip1;
private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem6;
private: System::Windows::Forms::ToolStripMenuItem^  duplicateToolStripMenuItem;
private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator1;
private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem7;
private: System::Windows::Forms::TextBox^  textBox1;
private: System::Windows::Forms::Button^  button1;
private: System::Windows::Forms::ComboBox^  comboBox2;
private: System::Windows::Forms::Label^  label8;
private: System::Windows::Forms::Label^  label7;
private: System::Windows::Forms::Label^  label1;
private: System::Windows::Forms::TextBox^  textBox8;
private: System::Windows::Forms::TextBox^  textBox7;
private: System::Windows::Forms::TextBox^  textBox6;
private: System::Windows::Forms::TextBox^  textBox5;
private: System::Windows::Forms::TextBox^  textBox4;
private: System::Windows::Forms::TextBox^  textBox3;
private: System::Windows::Forms::ComboBox^  comboBox1;
private: System::Windows::Forms::TextBox^  textBox2;
private: System::Windows::Forms::Label^  label15;
private: System::Windows::Forms::Label^  label14;
private: System::Windows::Forms::Label^  label13;
private: System::Windows::Forms::Label^  label12;
private: System::Windows::Forms::Label^  label11;
private: System::Windows::Forms::Label^  label10;
private: System::Windows::Forms::Label^  label9;
private: System::Windows::Forms::Button^  button2;
private: System::Windows::Forms::Label^  label16;
private: System::Windows::Forms::ComboBox^  comboBox4;
private: System::Windows::Forms::ComboBox^  comboBox3;
private: System::Windows::Forms::Label^  label17;
private: System::Windows::Forms::ComboBox^  comboBox6;
private: System::Windows::Forms::ComboBox^  comboBox5;
private: System::Windows::Forms::ComboBox^  comboBox8;
private: System::Windows::Forms::ComboBox^  comboBox7;
private: System::Windows::Forms::TextBox^  textBox11;
private: System::Windows::Forms::TextBox^  textBox10;
private: System::Windows::Forms::TextBox^  textBox9;
private: System::Windows::Forms::Label^  label18;
private: System::Windows::Forms::Button^  button3;
private: System::Windows::Forms::Label^  label19;
private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator2;
private: System::Windows::Forms::ToolStripMenuItem^  exportTextsToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem2;
private: System::ComponentModel::IContainer^  components;

		private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->menuStrip_mainMenu = (gcnew System::Windows::Forms::MenuStrip());
			this->toolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->loadToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->saveToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator2 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->analyzeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->exportTextsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->listBox_ActionController = (gcnew System::Windows::Forms::ListBox());
			this->contextMenuStrip1 = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
			this->toolStripMenuItem6 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->duplicateToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator1 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->toolStripMenuItem7 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->listBox_ActionSet = (gcnew System::Windows::Forms::ListBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->listBox_Procedures = (gcnew System::Windows::Forms::ListBox());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->textBox11 = (gcnew System::Windows::Forms::TextBox());
			this->textBox10 = (gcnew System::Windows::Forms::TextBox());
			this->textBox9 = (gcnew System::Windows::Forms::TextBox());
			this->label18 = (gcnew System::Windows::Forms::Label());
			this->comboBox8 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox7 = (gcnew System::Windows::Forms::ComboBox());
			this->label17 = (gcnew System::Windows::Forms::Label());
			this->comboBox6 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox5 = (gcnew System::Windows::Forms::ComboBox());
			this->label16 = (gcnew System::Windows::Forms::Label());
			this->comboBox4 = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox3 = (gcnew System::Windows::Forms::ComboBox());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->label15 = (gcnew System::Windows::Forms::Label());
			this->label14 = (gcnew System::Windows::Forms::Label());
			this->label13 = (gcnew System::Windows::Forms::Label());
			this->label12 = (gcnew System::Windows::Forms::Label());
			this->label11 = (gcnew System::Windows::Forms::Label());
			this->label10 = (gcnew System::Windows::Forms::Label());
			this->label9 = (gcnew System::Windows::Forms::Label());
			this->comboBox2 = (gcnew System::Windows::Forms::ComboBox());
			this->label8 = (gcnew System::Windows::Forms::Label());
			this->label7 = (gcnew System::Windows::Forms::Label());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->textBox8 = (gcnew System::Windows::Forms::TextBox());
			this->textBox7 = (gcnew System::Windows::Forms::TextBox());
			this->textBox6 = (gcnew System::Windows::Forms::TextBox());
			this->textBox5 = (gcnew System::Windows::Forms::TextBox());
			this->textBox4 = (gcnew System::Windows::Forms::TextBox());
			this->textBox3 = (gcnew System::Windows::Forms::TextBox());
			this->comboBox1 = (gcnew System::Windows::Forms::ComboBox());
			this->textBox2 = (gcnew System::Windows::Forms::TextBox());
			this->textBox_Condition = (gcnew System::Windows::Forms::TextBox());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->comboBox_cat = (gcnew System::Windows::Forms::ComboBox());
			this->label6 = (gcnew System::Windows::Forms::Label());
			this->button_search = (gcnew System::Windows::Forms::Button());
			this->textBox_pattern = (gcnew System::Windows::Forms::TextBox());
			this->comboBox_subCat = (gcnew System::Windows::Forms::ComboBox());
			this->comboBox_language = (gcnew System::Windows::Forms::ComboBox());
			this->textBox_translation = (gcnew System::Windows::Forms::TextBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->textBox1 = (gcnew System::Windows::Forms::TextBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->label19 = (gcnew System::Windows::Forms::Label());
			this->toolStripMenuItem2 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->menuStrip_mainMenu->SuspendLayout();
			this->contextMenuStrip1->SuspendLayout();
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->SuspendLayout();
			// 
			// menuStrip_mainMenu
			// 
			this->menuStrip_mainMenu->BackColor = System::Drawing::SystemColors::Control;
			this->menuStrip_mainMenu->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {this->toolStripMenuItem1});
			this->menuStrip_mainMenu->Location = System::Drawing::Point(0, 0);
			this->menuStrip_mainMenu->Name = L"menuStrip_mainMenu";
			this->menuStrip_mainMenu->Padding = System::Windows::Forms::Padding(0, 2, 2, 2);
			this->menuStrip_mainMenu->RenderMode = System::Windows::Forms::ToolStripRenderMode::System;
			this->menuStrip_mainMenu->Size = System::Drawing::Size(851, 24);
			this->menuStrip_mainMenu->TabIndex = 0;
			this->menuStrip_mainMenu->Text = L"menuStrip1";
			// 
			// toolStripMenuItem1
			// 
			this->toolStripMenuItem1->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
			this->toolStripMenuItem1->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(6) {this->loadToolStripMenuItem, 
				this->saveToolStripMenuItem, this->toolStripSeparator2, this->analyzeToolStripMenuItem, this->toolStripMenuItem2, this->exportTextsToolStripMenuItem});
			this->toolStripMenuItem1->ImageScaling = System::Windows::Forms::ToolStripItemImageScaling::None;
			this->toolStripMenuItem1->Name = L"toolStripMenuItem1";
			this->toolStripMenuItem1->Padding = System::Windows::Forms::Padding(0);
			this->toolStripMenuItem1->Size = System::Drawing::Size(29, 20);
			this->toolStripMenuItem1->Text = L"File";
			this->toolStripMenuItem1->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// loadToolStripMenuItem
			// 
			this->loadToolStripMenuItem->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
			this->loadToolStripMenuItem->Name = L"loadToolStripMenuItem";
			this->loadToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->loadToolStripMenuItem->Text = L"Load...";
			this->loadToolStripMenuItem->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->loadToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainWindow::click_load);
			// 
			// saveToolStripMenuItem
			// 
			this->saveToolStripMenuItem->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
			this->saveToolStripMenuItem->Name = L"saveToolStripMenuItem";
			this->saveToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->saveToolStripMenuItem->Text = L"Save";
			this->saveToolStripMenuItem->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->saveToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainWindow::click_export);
			// 
			// toolStripSeparator2
			// 
			this->toolStripSeparator2->Name = L"toolStripSeparator2";
			this->toolStripSeparator2->Size = System::Drawing::Size(149, 6);
			// 
			// analyzeToolStripMenuItem
			// 
			this->analyzeToolStripMenuItem->Name = L"analyzeToolStripMenuItem";
			this->analyzeToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->analyzeToolStripMenuItem->Text = L"Analyze";
			this->analyzeToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainWindow::click_analyze);
			// 
			// exportTextsToolStripMenuItem
			// 
			this->exportTextsToolStripMenuItem->Name = L"exportTextsToolStripMenuItem";
			this->exportTextsToolStripMenuItem->Size = System::Drawing::Size(152, 22);
			this->exportTextsToolStripMenuItem->Text = L"Export Texts";
			this->exportTextsToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainWindow::click_exportText);
			// 
			// listBox_ActionController
			// 
			this->listBox_ActionController->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left));
			this->listBox_ActionController->ContextMenuStrip = this->contextMenuStrip1;
			this->listBox_ActionController->FormattingEnabled = true;
			this->listBox_ActionController->Location = System::Drawing::Point(3, 58);
			this->listBox_ActionController->Name = L"listBox_ActionController";
			this->listBox_ActionController->Size = System::Drawing::Size(70, 355);
			this->listBox_ActionController->TabIndex = 2;
			this->listBox_ActionController->SelectedIndexChanged += gcnew System::EventHandler(this, &MainWindow::change_ActionController);
			// 
			// contextMenuStrip1
			// 
			this->contextMenuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {this->toolStripMenuItem6, 
				this->duplicateToolStripMenuItem, this->toolStripSeparator1, this->toolStripMenuItem7});
			this->contextMenuStrip1->Name = L"contextMenuStrip_SubCat";
			this->contextMenuStrip1->RenderMode = System::Windows::Forms::ToolStripRenderMode::System;
			this->contextMenuStrip1->ShowImageMargin = false;
			this->contextMenuStrip1->Size = System::Drawing::Size(100, 76);
			// 
			// toolStripMenuItem6
			// 
			this->toolStripMenuItem6->Name = L"toolStripMenuItem6";
			this->toolStripMenuItem6->Size = System::Drawing::Size(99, 22);
			this->toolStripMenuItem6->Text = L"Add";
			this->toolStripMenuItem6->Click += gcnew System::EventHandler(this, &MainWindow::click_addID);
			// 
			// duplicateToolStripMenuItem
			// 
			this->duplicateToolStripMenuItem->Name = L"duplicateToolStripMenuItem";
			this->duplicateToolStripMenuItem->Size = System::Drawing::Size(99, 22);
			this->duplicateToolStripMenuItem->Text = L"Duplicate";
			this->duplicateToolStripMenuItem->Click += gcnew System::EventHandler(this, &MainWindow::click_dupID);
			// 
			// toolStripSeparator1
			// 
			this->toolStripSeparator1->Name = L"toolStripSeparator1";
			this->toolStripSeparator1->Size = System::Drawing::Size(96, 6);
			// 
			// toolStripMenuItem7
			// 
			this->toolStripMenuItem7->Name = L"toolStripMenuItem7";
			this->toolStripMenuItem7->Size = System::Drawing::Size(99, 22);
			this->toolStripMenuItem7->Text = L"Delete";
			this->toolStripMenuItem7->Click += gcnew System::EventHandler(this, &MainWindow::click_deleteID);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label2->Location = System::Drawing::Point(76, 42);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(76, 13);
			this->label2->TabIndex = 3;
			this->label2->Text = L"Action Sets:";
			// 
			// listBox_ActionSet
			// 
			this->listBox_ActionSet->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left));
			this->listBox_ActionSet->ContextMenuStrip = this->contextMenuStrip1;
			this->listBox_ActionSet->FormattingEnabled = true;
			this->listBox_ActionSet->HorizontalScrollbar = true;
			this->listBox_ActionSet->Location = System::Drawing::Point(79, 58);
			this->listBox_ActionSet->Name = L"listBox_ActionSet";
			this->listBox_ActionSet->Size = System::Drawing::Size(180, 329);
			this->listBox_ActionSet->TabIndex = 4;
			this->listBox_ActionSet->SelectedIndexChanged += gcnew System::EventHandler(this, &MainWindow::change_ActionSet);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label3->Location = System::Drawing::Point(3, 12);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(64, 13);
			this->label3->TabIndex = 0;
			this->label3->Text = L"Condition:";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label4->Location = System::Drawing::Point(6, 154);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(75, 13);
			this->label4->TabIndex = 2;
			this->label4->Text = L"Procedures:";
			// 
			// listBox_Procedures
			// 
			this->listBox_Procedures->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->listBox_Procedures->ContextMenuStrip = this->contextMenuStrip1;
			this->listBox_Procedures->Font = (gcnew System::Drawing::Font(L"Courier New", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->listBox_Procedures->FormattingEnabled = true;
			this->listBox_Procedures->HorizontalScrollbar = true;
			this->listBox_Procedures->ItemHeight = 16;
			this->listBox_Procedures->Location = System::Drawing::Point(6, 170);
			this->listBox_Procedures->Name = L"listBox_Procedures";
			this->listBox_Procedures->Size = System::Drawing::Size(574, 132);
			this->listBox_Procedures->TabIndex = 3;
			this->listBox_Procedures->SelectedIndexChanged += gcnew System::EventHandler(this, &MainWindow::change_Procedures);
			// 
			// groupBox1
			// 
			this->groupBox1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom) 
				| System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->groupBox1->Controls->Add(this->button3);
			this->groupBox1->Controls->Add(this->textBox11);
			this->groupBox1->Controls->Add(this->textBox10);
			this->groupBox1->Controls->Add(this->textBox9);
			this->groupBox1->Controls->Add(this->label18);
			this->groupBox1->Controls->Add(this->comboBox8);
			this->groupBox1->Controls->Add(this->comboBox7);
			this->groupBox1->Controls->Add(this->label17);
			this->groupBox1->Controls->Add(this->comboBox6);
			this->groupBox1->Controls->Add(this->comboBox5);
			this->groupBox1->Controls->Add(this->label16);
			this->groupBox1->Controls->Add(this->comboBox4);
			this->groupBox1->Controls->Add(this->comboBox3);
			this->groupBox1->Controls->Add(this->button2);
			this->groupBox1->Controls->Add(this->label15);
			this->groupBox1->Controls->Add(this->label14);
			this->groupBox1->Controls->Add(this->label13);
			this->groupBox1->Controls->Add(this->label12);
			this->groupBox1->Controls->Add(this->label11);
			this->groupBox1->Controls->Add(this->label10);
			this->groupBox1->Controls->Add(this->label9);
			this->groupBox1->Controls->Add(this->comboBox2);
			this->groupBox1->Controls->Add(this->label8);
			this->groupBox1->Controls->Add(this->label7);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Controls->Add(this->textBox8);
			this->groupBox1->Controls->Add(this->textBox7);
			this->groupBox1->Controls->Add(this->textBox6);
			this->groupBox1->Controls->Add(this->textBox5);
			this->groupBox1->Controls->Add(this->textBox4);
			this->groupBox1->Controls->Add(this->textBox3);
			this->groupBox1->Controls->Add(this->comboBox1);
			this->groupBox1->Controls->Add(this->textBox2);
			this->groupBox1->Controls->Add(this->textBox_Condition);
			this->groupBox1->Controls->Add(this->label3);
			this->groupBox1->Controls->Add(this->listBox_Procedures);
			this->groupBox1->Controls->Add(this->label4);
			this->groupBox1->Location = System::Drawing::Point(265, 27);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(586, 410);
			this->groupBox1->TabIndex = 5;
			this->groupBox1->TabStop = false;
			// 
			// button3
			// 
			this->button3->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->button3->Location = System::Drawing::Point(397, 121);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(99, 23);
			this->button3->TabIndex = 45;
			this->button3->Text = L"Change";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &MainWindow::change_cvalues);
			// 
			// textBox11
			// 
			this->textBox11->Location = System::Drawing::Point(294, 123);
			this->textBox11->MaxLength = 12;
			this->textBox11->Name = L"textBox11";
			this->textBox11->Size = System::Drawing::Size(97, 20);
			this->textBox11->TabIndex = 44;
			// 
			// textBox10
			// 
			this->textBox10->Location = System::Drawing::Point(191, 123);
			this->textBox10->MaxLength = 12;
			this->textBox10->Name = L"textBox10";
			this->textBox10->Size = System::Drawing::Size(97, 20);
			this->textBox10->TabIndex = 43;
			// 
			// textBox9
			// 
			this->textBox9->Location = System::Drawing::Point(86, 123);
			this->textBox9->MaxLength = 12;
			this->textBox9->Name = L"textBox9";
			this->textBox9->Size = System::Drawing::Size(97, 20);
			this->textBox9->TabIndex = 42;
			// 
			// label18
			// 
			this->label18->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->label18->AutoSize = true;
			this->label18->Location = System::Drawing::Point(17, 126);
			this->label18->Name = L"label18";
			this->label18->Size = System::Drawing::Size(42, 13);
			this->label18->TabIndex = 41;
			this->label18->Text = L"Values:";
			// 
			// comboBox8
			// 
			this->comboBox8->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->comboBox8->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox8->FormattingEnabled = true;
			this->comboBox8->Items->AddRange(gcnew cli::array< System::Object^  >(3) {L"1", L"2", L"3"});
			this->comboBox8->Location = System::Drawing::Point(294, 72);
			this->comboBox8->Name = L"comboBox8";
			this->comboBox8->Size = System::Drawing::Size(79, 21);
			this->comboBox8->TabIndex = 40;
			// 
			// comboBox7
			// 
			this->comboBox7->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->comboBox7->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox7->FormattingEnabled = true;
			this->comboBox7->Items->AddRange(gcnew cli::array< System::Object^  >(3) {L"1", L"2", L"3"});
			this->comboBox7->Location = System::Drawing::Point(189, 72);
			this->comboBox7->Name = L"comboBox7";
			this->comboBox7->Size = System::Drawing::Size(79, 21);
			this->comboBox7->TabIndex = 39;
			// 
			// label17
			// 
			this->label17->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->label17->AutoSize = true;
			this->label17->Location = System::Drawing::Point(17, 99);
			this->label17->Name = L"label17";
			this->label17->Size = System::Drawing::Size(56, 13);
			this->label17->TabIndex = 38;
			this->label17->Text = L"Operators:";
			// 
			// comboBox6
			// 
			this->comboBox6->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->comboBox6->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox6->FormattingEnabled = true;
			this->comboBox6->Items->AddRange(gcnew cli::array< System::Object^  >(23) {L"Is_Timer_Ticking", L"Is_HP_Less", L"Is_Combat_Started", 
				L"Randomize", L"Is_Target_Killed", L"!", L"||", L"&&", L"Is_Dead", L"Is_Path_Reached", L"Attackers_Count_More", L"Distance_Range_More", 
				L">", L">=", L"<", L">", L">", L"<", L"==", L"Variable", L"Variable_Value", L"NPC_Refresh", L"Unknown"});
			this->comboBox6->Location = System::Drawing::Point(294, 96);
			this->comboBox6->Name = L"comboBox6";
			this->comboBox6->Size = System::Drawing::Size(99, 21);
			this->comboBox6->TabIndex = 37;
			// 
			// comboBox5
			// 
			this->comboBox5->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->comboBox5->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox5->FormattingEnabled = true;
			this->comboBox5->Items->AddRange(gcnew cli::array< System::Object^  >(23) {L"Is_Timer_Ticking", L"Is_HP_Less", L"Is_Combat_Started", 
				L"Randomize", L"Is_Target_Killed", L"!", L"||", L"&&", L"Is_Dead", L"Is_Path_Reached", L"Attackers_Count_More", L"Distance_Range_More", 
				L">", L">=", L"<", L">", L">", L"<", L"==", L"Variable", L"Variable_Value", L"NPC_Refresh", L"Unknown"});
			this->comboBox5->Location = System::Drawing::Point(189, 96);
			this->comboBox5->Name = L"comboBox5";
			this->comboBox5->Size = System::Drawing::Size(99, 21);
			this->comboBox5->TabIndex = 36;
			// 
			// label16
			// 
			this->label16->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->label16->AutoSize = true;
			this->label16->Location = System::Drawing::Point(17, 75);
			this->label16->Name = L"label16";
			this->label16->Size = System::Drawing::Size(61, 13);
			this->label16->TabIndex = 35;
			this->label16->Text = L"Cond. type:";
			// 
			// comboBox4
			// 
			this->comboBox4->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->comboBox4->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox4->FormattingEnabled = true;
			this->comboBox4->Items->AddRange(gcnew cli::array< System::Object^  >(3) {L"1", L"2", L"3"});
			this->comboBox4->Location = System::Drawing::Point(84, 72);
			this->comboBox4->Name = L"comboBox4";
			this->comboBox4->Size = System::Drawing::Size(79, 21);
			this->comboBox4->TabIndex = 34;
			// 
			// comboBox3
			// 
			this->comboBox3->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->comboBox3->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox3->FormattingEnabled = true;
			this->comboBox3->Items->AddRange(gcnew cli::array< System::Object^  >(23) {L"Is_Timer_Ticking", L"Is_HP_Less", L"Is_Combat_Started", 
				L"Randomize", L"Is_Target_Killed", L"!", L"||", L"&&", L"Is_Dead", L"Is_Path_Reached", L"Attackers_Count_More", L"Distance_Range_More", 
				L">", L">=", L"<", L">", L">", L"<", L"==", L"Variable", L"Variable_Value", L"NPC_Refresh", L"Unknown"});
			this->comboBox3->Location = System::Drawing::Point(84, 96);
			this->comboBox3->Name = L"comboBox3";
			this->comboBox3->Size = System::Drawing::Size(99, 21);
			this->comboBox3->TabIndex = 33;
			this->comboBox3->SelectedIndexChanged += gcnew System::EventHandler(this, &MainWindow::change_condition_option);
			// 
			// button2
			// 
			this->button2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->button2->Location = System::Drawing::Point(435, 382);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(99, 23);
			this->button2->TabIndex = 16;
			this->button2->Text = L"Change";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &MainWindow::change_arguments);
			// 
			// label15
			// 
			this->label15->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->label15->AutoSize = true;
			this->label15->Location = System::Drawing::Point(379, 368);
			this->label15->MaximumSize = System::Drawing::Size(50, 0);
			this->label15->MinimumSize = System::Drawing::Size(50, 0);
			this->label15->Name = L"label15";
			this->label15->Size = System::Drawing::Size(50, 13);
			this->label15->TabIndex = 32;
			this->label15->Text = L"7";
			this->label15->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			// 
			// label14
			// 
			this->label14->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->label14->AutoSize = true;
			this->label14->Location = System::Drawing::Point(316, 368);
			this->label14->MaximumSize = System::Drawing::Size(50, 0);
			this->label14->MinimumSize = System::Drawing::Size(50, 0);
			this->label14->Name = L"label14";
			this->label14->Size = System::Drawing::Size(50, 13);
			this->label14->TabIndex = 31;
			this->label14->Text = L"6";
			this->label14->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			// 
			// label13
			// 
			this->label13->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->label13->AutoSize = true;
			this->label13->Location = System::Drawing::Point(247, 368);
			this->label13->MaximumSize = System::Drawing::Size(50, 0);
			this->label13->MinimumSize = System::Drawing::Size(50, 0);
			this->label13->Name = L"label13";
			this->label13->Size = System::Drawing::Size(50, 13);
			this->label13->TabIndex = 30;
			this->label13->Text = L"5";
			this->label13->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			// 
			// label12
			// 
			this->label12->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->label12->AutoSize = true;
			this->label12->Location = System::Drawing::Point(186, 368);
			this->label12->MaximumSize = System::Drawing::Size(50, 0);
			this->label12->MinimumSize = System::Drawing::Size(50, 0);
			this->label12->Name = L"label12";
			this->label12->Size = System::Drawing::Size(50, 13);
			this->label12->TabIndex = 29;
			this->label12->Text = L"4";
			this->label12->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			// 
			// label11
			// 
			this->label11->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->label11->AutoSize = true;
			this->label11->Location = System::Drawing::Point(125, 368);
			this->label11->MaximumSize = System::Drawing::Size(50, 0);
			this->label11->MinimumSize = System::Drawing::Size(50, 0);
			this->label11->Name = L"label11";
			this->label11->Size = System::Drawing::Size(50, 13);
			this->label11->TabIndex = 28;
			this->label11->Text = L"3";
			this->label11->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			// 
			// label10
			// 
			this->label10->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->label10->AutoSize = true;
			this->label10->Location = System::Drawing::Point(67, 368);
			this->label10->MaximumSize = System::Drawing::Size(50, 0);
			this->label10->MinimumSize = System::Drawing::Size(50, 0);
			this->label10->Name = L"label10";
			this->label10->Size = System::Drawing::Size(50, 13);
			this->label10->TabIndex = 27;
			this->label10->Text = L"2";
			this->label10->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			// 
			// label9
			// 
			this->label9->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->label9->AutoSize = true;
			this->label9->Location = System::Drawing::Point(7, 368);
			this->label9->MaximumSize = System::Drawing::Size(50, 0);
			this->label9->MinimumSize = System::Drawing::Size(50, 0);
			this->label9->Name = L"label9";
			this->label9->Size = System::Drawing::Size(50, 13);
			this->label9->TabIndex = 26;
			this->label9->Text = L"1";
			this->label9->TextAlign = System::Drawing::ContentAlignment::TopCenter;
			// 
			// comboBox2
			// 
			this->comboBox2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->comboBox2->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox2->FormattingEnabled = true;
			this->comboBox2->Items->AddRange(gcnew cli::array< System::Object^  >(8) {L"AGGRO_FIRST", L"AGGRO_SECOND", L"AGGRO_SECOND_RAND", 
				L"MOST_HP", L"MOST_MP", L"LEAST_HP", L"CLASS_COMBO", L"SELF"});
			this->comboBox2->Location = System::Drawing::Point(434, 321);
			this->comboBox2->Name = L"comboBox2";
			this->comboBox2->Size = System::Drawing::Size(99, 21);
			this->comboBox2->TabIndex = 16;
			this->comboBox2->SelectedIndexChanged += gcnew System::EventHandler(this, &MainWindow::change_Procedure_target);
			// 
			// label8
			// 
			this->label8->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->label8->AutoSize = true;
			this->label8->Location = System::Drawing::Point(432, 305);
			this->label8->Name = L"label8";
			this->label8->Size = System::Drawing::Size(88, 13);
			this->label8->TabIndex = 25;
			this->label8->Text = L"Target argument:";
			// 
			// label7
			// 
			this->label7->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->label7->AutoSize = true;
			this->label7->Location = System::Drawing::Point(6, 347);
			this->label7->Name = L"label7";
			this->label7->Size = System::Drawing::Size(111, 13);
			this->label7->TabIndex = 24;
			this->label7->Text = L"Procedure agruments:";
			// 
			// label1
			// 
			this->label1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(7, 305);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(99, 13);
			this->label1->TabIndex = 23;
			this->label1->Text = L"Change Procedure:";
			// 
			// textBox8
			// 
			this->textBox8->Location = System::Drawing::Point(373, 383);
			this->textBox8->MaxLength = 500;
			this->textBox8->Name = L"textBox8";
			this->textBox8->Size = System::Drawing::Size(55, 20);
			this->textBox8->TabIndex = 22;
			// 
			// textBox7
			// 
			this->textBox7->Location = System::Drawing::Point(311, 383);
			this->textBox7->MaxLength = 500;
			this->textBox7->Name = L"textBox7";
			this->textBox7->Size = System::Drawing::Size(55, 20);
			this->textBox7->TabIndex = 21;
			// 
			// textBox6
			// 
			this->textBox6->Location = System::Drawing::Point(250, 383);
			this->textBox6->MaxLength = 500;
			this->textBox6->Name = L"textBox6";
			this->textBox6->Size = System::Drawing::Size(55, 20);
			this->textBox6->TabIndex = 20;
			// 
			// textBox5
			// 
			this->textBox5->Location = System::Drawing::Point(189, 383);
			this->textBox5->MaxLength = 500;
			this->textBox5->Name = L"textBox5";
			this->textBox5->Size = System::Drawing::Size(55, 20);
			this->textBox5->TabIndex = 19;
			// 
			// textBox4
			// 
			this->textBox4->Location = System::Drawing::Point(128, 383);
			this->textBox4->MaxLength = 500;
			this->textBox4->Name = L"textBox4";
			this->textBox4->Size = System::Drawing::Size(55, 20);
			this->textBox4->TabIndex = 18;
			// 
			// textBox3
			// 
			this->textBox3->Location = System::Drawing::Point(67, 384);
			this->textBox3->MaxLength = 500;
			this->textBox3->Name = L"textBox3";
			this->textBox3->Size = System::Drawing::Size(55, 20);
			this->textBox3->TabIndex = 17;
			// 
			// comboBox1
			// 
			this->comboBox1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->comboBox1->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox1->FormattingEnabled = true;
			this->comboBox1->Items->AddRange(gcnew cli::array< System::Object^  >(33) {L"Attack", L"Cast_Skill", L"Broadcast_Message", 
				L"Reset_Aggro", L"Execute_ActionSet", L"Disable_ActionSet", L"Enable_ActionSet", L"Create_Timer", L"Remove_Timer", L"Run_Away", 
				L"Be_Taunted", L"Fade_Target", L"Fade_Aggro", L"Break", L"NPC_Generator", L"Creature_Generator", L"Increment_Public_Counter", 
				L"Player_Aimed_NPC_Spawn", L"Change_Path", L"Play_Action", L"Unknown1", L"Unknown2", L"Unknown3", L"Unknown4", L"Unknown5", L"Unknown6", 
				L"Unknown7", L"Unknown8", L"Unknown9", L"Unknown10", L"Unknown11", L"Unknown12", L"Unknown13"});
			this->comboBox1->Location = System::Drawing::Point(9, 321);
			this->comboBox1->Name = L"comboBox1";
			this->comboBox1->Size = System::Drawing::Size(419, 21);
			this->comboBox1->TabIndex = 16;
			this->comboBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &MainWindow::change_Procedure_type);
			// 
			// textBox2
			// 
			this->textBox2->Location = System::Drawing::Point(6, 384);
			this->textBox2->MaxLength = 500;
			this->textBox2->Name = L"textBox2";
			this->textBox2->Size = System::Drawing::Size(55, 20);
			this->textBox2->TabIndex = 16;
			// 
			// textBox_Condition
			// 
			this->textBox_Condition->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->textBox_Condition->Cursor = System::Windows::Forms::Cursors::Default;
			this->textBox_Condition->Font = (gcnew System::Drawing::Font(L"Courier New", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->textBox_Condition->Location = System::Drawing::Point(10, 28);
			this->textBox_Condition->Multiline = true;
			this->textBox_Condition->Name = L"textBox_Condition";
			this->textBox_Condition->ScrollBars = System::Windows::Forms::ScrollBars::Horizontal;
			this->textBox_Condition->Size = System::Drawing::Size(564, 44);
			this->textBox_Condition->TabIndex = 1;
			this->textBox_Condition->WordWrap = false;
			// 
			// groupBox2
			// 
			this->groupBox2->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->groupBox2->Controls->Add(this->comboBox_cat);
			this->groupBox2->Controls->Add(this->label6);
			this->groupBox2->Controls->Add(this->button_search);
			this->groupBox2->Controls->Add(this->textBox_pattern);
			this->groupBox2->Controls->Add(this->comboBox_subCat);
			this->groupBox2->Location = System::Drawing::Point(3, 469);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(845, 44);
			this->groupBox2->TabIndex = 8;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Search";
			// 
			// comboBox_cat
			// 
			this->comboBox_cat->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->comboBox_cat->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox_cat->FormattingEnabled = true;
			this->comboBox_cat->Items->AddRange(gcnew cli::array< System::Object^  >(4) {L"AI Control Link", L"Conditions", L"Procedures", 
				L"Targets"});
			this->comboBox_cat->Location = System::Drawing::Point(9, 14);
			this->comboBox_cat->Name = L"comboBox_cat";
			this->comboBox_cat->Size = System::Drawing::Size(103, 21);
			this->comboBox_cat->TabIndex = 0;
			this->comboBox_cat->SelectedIndexChanged += gcnew System::EventHandler(this, &MainWindow::change_searchCat);
			// 
			// label6
			// 
			this->label6->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->label6->AutoSize = true;
			this->label6->Location = System::Drawing::Point(263, 17);
			this->label6->Name = L"label6";
			this->label6->Size = System::Drawing::Size(60, 13);
			this->label6->TabIndex = 2;
			this->label6->Text = L"Containing:";
			// 
			// button_search
			// 
			this->button_search->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->button_search->Location = System::Drawing::Point(761, 12);
			this->button_search->Name = L"button_search";
			this->button_search->Size = System::Drawing::Size(75, 23);
			this->button_search->TabIndex = 4;
			this->button_search->Text = L"Find Next";
			this->button_search->UseVisualStyleBackColor = true;
			this->button_search->Click += gcnew System::EventHandler(this, &MainWindow::click_search);
			// 
			// textBox_pattern
			// 
			this->textBox_pattern->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->textBox_pattern->Location = System::Drawing::Point(329, 14);
			this->textBox_pattern->Name = L"textBox_pattern";
			this->textBox_pattern->Size = System::Drawing::Size(426, 20);
			this->textBox_pattern->TabIndex = 3;
			// 
			// comboBox_subCat
			// 
			this->comboBox_subCat->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->comboBox_subCat->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox_subCat->FormattingEnabled = true;
			this->comboBox_subCat->Location = System::Drawing::Point(118, 14);
			this->comboBox_subCat->Name = L"comboBox_subCat";
			this->comboBox_subCat->Size = System::Drawing::Size(139, 21);
			this->comboBox_subCat->TabIndex = 1;
			// 
			// comboBox_language
			// 
			this->comboBox_language->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->comboBox_language->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->comboBox_language->FormattingEnabled = true;
			this->comboBox_language->Location = System::Drawing::Point(6, 442);
			this->comboBox_language->Name = L"comboBox_language";
			this->comboBox_language->Size = System::Drawing::Size(70, 21);
			this->comboBox_language->TabIndex = 6;
			// 
			// textBox_translation
			// 
			this->textBox_translation->Anchor = static_cast<System::Windows::Forms::AnchorStyles>(((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left) 
				| System::Windows::Forms::AnchorStyles::Right));
			this->textBox_translation->Location = System::Drawing::Point(82, 443);
			this->textBox_translation->Name = L"textBox_translation";
			this->textBox_translation->Size = System::Drawing::Size(769, 20);
			this->textBox_translation->TabIndex = 7;
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->label5->Location = System::Drawing::Point(0, 42);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(71, 13);
			this->label5->TabIndex = 9;
			this->label5->Text = L"Controllers:";
			// 
			// textBox1
			// 
			this->textBox1->Location = System::Drawing::Point(79, 412);
			this->textBox1->MaxLength = 100;
			this->textBox1->Name = L"textBox1";
			this->textBox1->Size = System::Drawing::Size(127, 20);
			this->textBox1->TabIndex = 15;
			// 
			// button1
			// 
			this->button1->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Right));
			this->button1->Location = System::Drawing::Point(207, 411);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(53, 23);
			this->button1->TabIndex = 5;
			this->button1->Text = L"Change";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MainWindow::rename_ac);
			// 
			// label19
			// 
			this->label19->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Bottom | System::Windows::Forms::AnchorStyles::Left));
			this->label19->AutoSize = true;
			this->label19->Location = System::Drawing::Point(79, 395);
			this->label19->Name = L"label19";
			this->label19->Size = System::Drawing::Size(38, 13);
			this->label19->TabIndex = 46;
			this->label19->Text = L"Name:";
			// 
			// toolStripMenuItem2
			// 
			this->toolStripMenuItem2->Name = L"toolStripMenuItem2";
			this->toolStripMenuItem2->Size = System::Drawing::Size(152, 22);
			this->toolStripMenuItem2->Text = L"Import Texts";
			this->toolStripMenuItem2->Click += gcnew System::EventHandler(this, &MainWindow::click_importText);
			// 
			// MainWindow
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(851, 516);
			this->Controls->Add(this->label19);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->textBox_translation);
			this->Controls->Add(this->textBox1);
			this->Controls->Add(this->comboBox_language);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->listBox_ActionSet);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->listBox_ActionController);
			this->Controls->Add(this->menuStrip_mainMenu);
			this->Name = L"MainWindow";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L" jAIedit mod by CompeR";
			this->menuStrip_mainMenu->ResumeLayout(false);
			this->menuStrip_mainMenu->PerformLayout();
			this->contextMenuStrip1->ResumeLayout(false);
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

		Collections::SortedList^ loadSkillReplace(String^ file)
		{
			Collections::SortedList^ skillTable = gcnew Collections::SortedList();

			String^ debug = Application::StartupPath + "\\" + file;
			StreamReader^ sr = gcnew StreamReader(Application::StartupPath + "\\" + file);

			String^ line;
			array<String^>^ pair;
			array<String^>^ seperator = gcnew array<String^>{"="};
			while(!sr->EndOfStream)
			{
				line = sr->ReadLine();
				if(!line->StartsWith("#") && line->Contains("="))
				{
					pair = line->Split(seperator, StringSplitOptions::RemoveEmptyEntries);
					if(pair->Length == 2)
					{
						skillTable->Add(Convert::ToInt32(pair[0]), Convert::ToInt32(pair[1]));
					}
				}
			}

			sr->Close();

			return skillTable;
		}

		ArrayList^ loadBlockDisable(String^ file)
		{
			ArrayList^ blockTable = gcnew ArrayList();

			String^ debug = Application::StartupPath + "\\" + file;
			StreamReader^ sr = gcnew StreamReader(Application::StartupPath + "\\" + file);

			String^ line;
			array<String^>^ pair;
			array<String^>^ seperator = gcnew array<String^>{":"};
			while(!sr->EndOfStream)
			{
				line = sr->ReadLine();
				if(!line->StartsWith("#") && line->Contains(":"))
				{
					pair = line->Split(seperator, StringSplitOptions::RemoveEmptyEntries);
					if(pair->Length == 2)
					{
						blockTable->Add(pair[0] + ":" + pair[1]);
					}
				}
			}

			sr->Close();

			return blockTable;
		}
		String^ condition_name(int operator_id)
		{
			if(operator_id == 0)
			{
				return "Is_Timer_Ticking";
			}
			if(operator_id == 1)
			{
				return "Is_HP_Less";
			}
			if(operator_id == 2)
			{
				return "Is_Combat_Started";
			}
			if(operator_id == 3)
			{
				return "Randomize";
			}
			if(operator_id == 4)
			{
				return "Is_Target_Killed";
			}
			if(operator_id == 5)
			{
				return "!"; // NOT
			}
			if(operator_id == 6)
			{
				return "||"; // OR
			}
			if(operator_id == 7)
			{
				return "&&"; // AND
			}
			if(operator_id == 8)
			{
				return "Is_Dead";
			}
			if(operator_id == 9)
			{
				return "Is_Path_Reached";
			}
			if(operator_id == 10)
			{
				return "Attackers_Count_More"; 
			}
			if(operator_id == 11)
			{
				return "Distance_Range_More"; 
			}
			if(operator_id == 12)
			{
				return ">"; // ?
			}
			if(operator_id == 13)
			{
				return ">="; // GREATER EQUALS ?
			}
			if(operator_id == 14)
			{
				return "<"; // LESS THEN
			}
			if(operator_id == 15)
			{
				return ">"; // GREATER THEN
			}
			if(operator_id == 16)
			{
				return ">"; // GREATER THEN
			}
			if(operator_id == 17)
			{
				return "<";
			}
			if(operator_id == 18)
			{
				return "==";
			}
			if(operator_id == 19)
			{
				return "Variable";
			}
			if(operator_id == 20)
			{
				return "Variable_Value";
			}
			if(operator_id == 21)
			{
				return "Top";
			}
			if(operator_id == 22)
			{
				return "NPC_Refresh";
			}
			if(operator_id == 23)
			{
				return "Unknown";
			}
			return "?c"+operator_id;
		}
		String^ condition_value(Condition^ c)
		{
			if(c->operator_id == 0)
			{
				return BitConverter::ToInt32(c->value, 0).ToString();
			}
			if(c->operator_id == 1)
			{
				return BitConverter::ToSingle(c->value, 0).ToString("F2");
			}
			if(c->operator_id == 2)
			{
				return "";
			}
			if(c->operator_id == 3)
			{
				return BitConverter::ToSingle(c->value, 0).ToString("F2");
			}
			if(c->operator_id == 4)
			{
				return "";
			}
			if(c->operator_id == 5)
			{
				return "NOT";
			}
			if(c->operator_id == 6)
			{
				return "";
			}
			if(c->operator_id == 7)
			{
				return "";
			}
			if(c->operator_id == 8)
			{
				return "";
			}
			if(c->operator_id == 9)
			{
				return BitConverter::ToInt32(c->value, 0).ToString();
			}
			if(c->operator_id == 10)
			{
				return BitConverter::ToInt32(c->value, 0).ToString();
			}
			if(c->operator_id == 11)
			{
				return BitConverter::ToSingle(c->value, 0).ToString("F2");
			}
			if(c->operator_id == 12)
			{
				return "";
			}
			if(c->operator_id == 13)
			{
				return "";
			}
			if(c->operator_id == 14)
			{
				return "";
			}
			if(c->operator_id == 15)
			{
				return "";
			}
			if(c->operator_id == 16)
			{
				return ""; //BitConverter::ToInt32(c->value, 0).ToString();
			}
			if(c->operator_id == 17)
			{
				return "";
			}
			if(c->operator_id == 18)
			{
				return "";
			}
			if(c->operator_id == 19)
			{
				return BitConverter::ToInt32(c->value, 0).ToString();
			}
			if(c->operator_id == 20)
			{
				return BitConverter::ToInt32(c->value, 0).ToString();
			}
			if(c->operator_id == 21)
			{
				return "";
			}
			if(c->operator_id == 22)
			{
				return "";
			}
			return "?";
		}
		array<unsigned char>^ condition_value_convert(int operator_id, String^ value)
		{
			if(operator_id == 0)
			{
				return BitConverter::GetBytes(Convert::ToInt32(value)); // BitConverter::GetBytes(
			}
			if(operator_id == 1)
			{
				return BitConverter::GetBytes(Convert::ToSingle(value));
			}
			//if(operator_id == 2)
			//{
			//	return "";
			//}
			if(operator_id == 3)
			{
				return BitConverter::GetBytes(Convert::ToSingle(value));
			}
			//if(operator_id == 4)
			//{
			//	return "";
			//}
			//if(operator_id == 5)
			//{
			//	return "NOT";
			//}
			//if(operator_id == 6)
			//{
			//	return "";
			//}
			//if(operator_id == 7)
			//{
			//	return "";
			//}
			//if(operator_id == 8)
			//{
			//	return "";
			//}
			if(operator_id == 9)
			{
				return BitConverter::GetBytes(Convert::ToInt32(value));
			}
			if(operator_id == 10)
			{
				return BitConverter::GetBytes(Convert::ToInt32(value));
			}
			if(operator_id == 11)
			{
				return BitConverter::GetBytes(Convert::ToSingle(value));
			}
			//if(operator_id == 12)
			//{
			//	return "";
			//}
			//if(operator_id == 13)
			//{
			//	return "";
			//}
			//if(operator_id == 14)
			//{
			//	return "";
			//}
			//if(operator_id == 15)
			//{
			//	return "";
			//}
			//if(operator_id == 16)
			//{
			//	return "";
			//}
			//if(operator_id == 17)
			//{
			//	return BitConverter::GetBytes(Convert::ToInt32(value));
			//}
			//if(operator_id == 18)
			//{
			//	return "";
			//}
			if(operator_id == 19)
			{
				return BitConverter::GetBytes(Convert::ToInt32(value));
			}
			if(operator_id == 20)
			{
				return BitConverter::GetBytes(Convert::ToInt32(value));
			}
			//if(operator_id == 21)
			//{
			//	return "";
			//}
			//if(operator_id == 22)
			//{
			//	return "";
			//}
			//return "?";
			return BitConverter::GetBytes(Convert::ToInt32("0"));
		}
		String^ condition_expression(Condition^ c)
		{
			String^ expression = "";

			if(c->condition_type == 1)
			{
				expression += "(";
				expression += condition_expression(c->condition_left);
				expression += " " + condition_name(c->operator_id) + " ";
				expression += condition_expression(c->condition_right);
				expression += ")";
			}
			if(c->condition_type == 2)
			{
				expression += condition_name(c->operator_id);
				expression += "(";
				expression += condition_expression(c->condition_right);
				expression += ")";
			}
			if(c->condition_type > 2)
			{
				expression += condition_name(c->operator_id);
				expression += "(";
				if(c->argument_bytes > 0)
				{
					expression += condition_value(c);
				}
				expression += ")";
			}

			return expression;
		}
		String^ procedure_target(int target, array<Object^>^ target_parmeters)
		{
			String^ expression = "Target(";

			if(target == 0)
			{
				expression += "AGGRO_FIRST";
			}
			if(target == 1)
			{
				expression += "AGGRO_SECOND";
			}
			if(target == 2)
			{
				expression += "AGGRO_SECOND_RAND";
			}
			if(target == 3)
			{
				expression += "MOST_HP";
			}
			if(target == 4)
			{
				expression += "MOST_MP";
			}
			if(target == 5)
			{
				expression += "LEAST_HP";
			}
			if(target == 6)
			{
				expression += "CLASS_COMBO";
				expression += ", " + ((int)target_parmeters[0]).ToString();
			}
			if(target == 7)
			{
				expression += "SELF";
			}
			//comper
			if(target == 11)
			{
				expression += "SELF";
			}

			expression += ")";

			return expression;
		}
		String^ procedure_expression(Procedure^ p)
		{
			String^ expression = "";

			// op_attack(int _attack_strategy)
			if(p->type == 0)
			{
				expression = "Attack(";
				expression += ((int)p->parameters[0]).ToString();
				expression += ")";
			}
			// op_skill(int _skill_id, int _skill_lvl)
			if(p->type == 1)
			{
				expression = "Cast_Skill(";
				expression += ((int)p->parameters[0]).ToString() + ", ";
				expression += ((int)p->parameters[1]).ToString();
				expression += ")";
			}
			// op_say(void *_msg, size_t _size)
			if(p->type == 2)
			{
				expression = "Broadcast_Message(";
				expression += ((int)p->parameters[0]).ToString() + ", ";
				expression += "\"" + (Encoding::Unicode)->GetString((array<unsigned char>^)p->parameters[1])->Replace("\0", "") + "\"";
				expression += ")";
			}
			// op_reset_aggro(void)
			if(p->type == 3)
			{
				expression = "Reset_Aggro()";
			}
			// op_exec_trigger(ai_trigger::trigger *)
			if(p->type == 4)
			{
				expression = "Execute_ActionSet(";
				expression += ((int)p->parameters[0]).ToString();
				expression += ")";
			}
			// op_enable_trigger(int _trigger_id, bool _is_enable)
			// _is_enable = false
			if(p->type == 5)
			{
				expression = "Disable_ActionSet(";
				expression += ((int)p->parameters[0]).ToString();
				expression += ")";
			}
			// op_enable_trigger(int _trigger_id, bool _is_enable)
			// _is_enable = true
			if(p->type == 6)
			{
				expression = "Enable_ActionSet(";
				expression += ((int)p->parameters[0]).ToString();
				expression += ")";
			}
			// op_create_timer(int _timerid, int _interval, int _count)
			if(p->type == 7)
			{
				expression = "Create_Timer(";
				expression += ((int)p->parameters[0]).ToString() + ", ";
				expression += ((int)p->parameters[1]).ToString() + ", ";
				expression += ((int)p->parameters[2]).ToString();
				expression += ")";
			}
			// op_remove_timer(int _timerid)
			if(p->type == 8)
			{
				expression = "Remove_Timer(";
				expression += ((int)p->parameters[0]).ToString();
				expression += ")";
			}
			// op_flee(void)
			if(p->type == 9)
			{
				expression = "Run_Away()";
			}
			// op_be_taunted(void)
			if(p->type == 10)
			{
				expression = "Be_Taunted()";
			}
			// op_fade_target(void)
			if(p->type == 11)
			{
				expression = "Fade_Target()";
			}
			// op_aggro_fade(void)
			if(p->type == 12)
			{
				expression = "Fade_Aggro()";
			}
			// op_break(void)
			if(p->type == 13)
			{
				expression = "Break()";
			}
			// op_active_spawner(int _ctrl_id, bool _is_active_spawner)
			if(p->type == 14)
			{
				expression = "NPC_Generator(";
				expression += ((int)p->parameters[0]).ToString() + ", ";
				expression += ((__int16)p->parameters[1]).ToString() + " ,";
				expression += ((__int16)p->parameters[2]).ToString();
				expression += ")";
			}
			// op_set_common_data(int _key, int _set_value, bool _is_value)
			if(p->type == 15)
			{
				expression = "Creature_Generator(";
				expression += ((int)p->parameters[0]).ToString() + ", ";
				expression += ((int)p->parameters[1]).ToString() + ", ";
				expression += ((int)p->parameters[2]).ToString() + ", ";
				expression += ((int)p->parameters[3]).ToString() + ", ";
				expression += "\"" + (Encoding::Unicode)->GetString((array<unsigned char>^)p->parameters[4])->Replace("\0", "") + "\",";
				expression += ((int)p->parameters[5]).ToString() + ", ";
				expression += ((int)p->parameters[6]).ToString();
				expression += ")";
			}
			// op_add_common_data(int _key, int _add_value)
			if(p->type == 16)
			{
				expression = "Increment_Public_Counter(";
				expression += ((int)p->parameters[0]).ToString();
				expression += ")";
			}
			// op_summon_monster(int _mob_id, int _count, int _target_distance, int _remain_time, char _die_with_who, int _path_id)
			if(p->type == 17)
			{
				expression = "Player_Aimed_NPC_Spawn(";
				expression += ((int)p->parameters[0]).ToString() + ", ";
				expression += ((int)p->parameters[1]).ToString();
				expression += ")";
			}
			// op_change_path(int _world_tag, int _global_path_id, int _path_type, char _speed_flag)
			if(p->type == 18)
			{
				expression = "Change_Path(";
				//expression += ((int)p->parameters[0]).ToString() + ", ";
				//expression += ((int)p->parameters[1]).ToString() + ", ";
				//expression += ((int)p->parameters[2]).ToString() + ", ";
				//expression += ((int)p->parameters[3]).ToString();
				expression += ")";
			}
			// op_play_action(char _action_name[128], int _play_times, int _action_last_time, int _interval_time)
			if(p->type == 19)
			{
				expression = "Play_Action(";
				expression += "\"" + (Encoding::GetEncoding("GBK"))->GetString((array<unsigned char>^)p->parameters[0])->Replace("\0", "") + "\", ";
				expression += ((int)p->parameters[1]).ToString() + ", ";
				expression += ((int)p->parameters[2]).ToString() + ", ";
				expression += ((int)p->parameters[3]).ToString();
				expression += ")";
			}
			if(p->type == 20)
			{
				expression = "Unknown1(";
				
				expression += ")";
			}
			if(p->type == 21)
			{
				expression = "Unknown2(";
				
				expression += ")";
			}
			if(p->type == 22)
			{
				expression = "Unknown3(";
				expression += ((int)p->parameters[0]).ToString() + ", ";
				expression += ((int)p->parameters[1]).ToString();
				expression += ")";
			}
			if(p->type == 23)
			{
				expression = "Unknown4(";
				expression += ((int)p->parameters[0]).ToString() + ", ";
				expression += ((int)p->parameters[1]).ToString();
				expression += ")";
			}
			if(p->type == 24)
			{
				expression = "Unknown5(";
				
				expression += ")";
			}
			if(p->type == 25)
			{
				expression = "Unknown6(";
				expression += ((int)p->parameters[0]).ToString() + ", ";
				expression += ((int)p->parameters[1]).ToString() + ", ";
				expression += ((int)p->parameters[2]).ToString() + ", ";
				expression += ((__int16)p->parameters[3]).ToString() + ", ";
				expression += ((__int16)p->parameters[4]).ToString() + ", ";
				expression += ((int)p->parameters[5]).ToString();
				expression += ")";
			}
			if(p->type == 26)
			{
				expression = "Unknown7(";
				expression += ((int)p->parameters[0]).ToString() + ", ";
				expression += ((int)p->parameters[1]).ToString() + ", ";
				expression += ((int)p->parameters[2]).ToString();
				expression += ")";
			}
			if(p->type == 27)
			{
				expression = "Unknown8(";
				expression += ")";
			}
			if(p->type == 28)
			{
				expression = "Unknown9(";
				expression += ((int)p->parameters[0]).ToString();
				expression += ")";
			}
			if(p->type == 29)
			{
				expression = "Unknown10(";
				expression += ((int)p->parameters[0]).ToString();
				expression += ")";
			}
			if(p->type == 30)
			{
				expression = "Unknown11(";

				expression += ")";
			}
			/*
			op_swap_aggro(unsigned int, unsigned int);
			size_t _index1;
			size_t _index2;
			*/

			expression += " " + procedure_target(p->target_type, p->target_parameters);

			return expression;
		}
		void procedure_show(Procedure^ p)
		{
			// op_attack(int _attack_strategy)
			if(p->type == 0)
			{
				label9->Text="att_tact";
				textBox2->Text=((int)p->parameters[0]).ToString();
				textBox2->Enabled= true;
			}
			// op_skill(int _skill_id, int _skill_lvl)
			if(p->type == 1)
			{
				label9->Text="skill_id";
				label10->Text="skill_lvl";
				textBox2->Text=((int)p->parameters[0]).ToString();
				textBox3->Text=((int)p->parameters[1]).ToString();
				textBox2->Enabled= true;
				textBox3->Enabled= true;
			}
			// op_say(void *_msg, size_t _size)
			if(p->type == 2)
			{
				label9->Text="m_size";
				label10->Text="message";
				textBox2->Text=((int)p->parameters[0]).ToString();
				textBox3->Text=(Encoding::Unicode)->GetString((array<unsigned char>^)p->parameters[1])->Replace("\0", "");
				//textBox2->Enabled= true;
				textBox3->Enabled= true;
			}
			// op_reset_aggro(void)
			if(p->type == 3)
			{
				//expression = "Reset_Aggro()";
			}
			// op_exec_trigger(ai_trigger::trigger *)
			if(p->type == 4)
			{
				label9->Text="ai_tri";
				textBox2->Text=((int)p->parameters[0]).ToString();
				textBox2->Enabled= true;
			}
			// op_enable_trigger(int _trigger_id, bool _is_enable)
			// _is_enable = false
			if(p->type == 5)
			{
				label9->Text="ai_tri";
				textBox2->Text=((int)p->parameters[0]).ToString();
				textBox2->Enabled= true;
			}
			// op_enable_trigger(int _trigger_id, bool _is_enable)
			// _is_enable = true
			if(p->type == 6)
			{
				label9->Text="ai_tri";
				textBox2->Text=((int)p->parameters[0]).ToString();
				textBox2->Enabled= true;
			}
			// op_create_timer(int _timerid, int _interval, int _count)
			if(p->type == 7)
			{
				label9->Text="timer_id";
				textBox2->Text=((int)p->parameters[0]).ToString();
				label10->Text="inverval";
				textBox3->Text=((int)p->parameters[1]).ToString();
				label11->Text="count";
				textBox4->Text=((int)p->parameters[2]).ToString();
				textBox2->Enabled= true;
				textBox3->Enabled= true;
				textBox4->Enabled= true;
			}
			// op_remove_timer(int _timerid)
			if(p->type == 8)
			{
				label9->Text="tim_id";
				textBox2->Text=((int)p->parameters[0]).ToString();
				textBox2->Enabled= true;
			}
			// op_flee(void)
			if(p->type == 9)
			{
				//expression = "Run_Away()";
			}
			// op_be_taunted(void)
			if(p->type == 10)
			{
				//expression = "Be_Taunted()";
			}
			// op_fade_target(void)
			if(p->type == 11)
			{
				//expression = "Fade_Target()";
			}
			// op_aggro_fade(void)
			if(p->type == 12)
			{
				//expression = "Fade_Aggro()";
			}
			// op_break(void)
			if(p->type == 13)
			{
				//expression = "Break()";
			}
			// op_active_spawner(int _ctrl_id, bool _is_active_spawner)
			if(p->type == 14)
			{
				label9->Text="ctrl_id";
				textBox2->Text=((int)p->parameters[0]).ToString();
				label10->Text="unk";
				textBox3->Text=((__int16)p->parameters[1]).ToString();
				label11->Text="unk";
				textBox4->Text=((__int16)p->parameters[2]).ToString();
				textBox2->Enabled= true;
				textBox3->Enabled= true;
				textBox4->Enabled= true;
			}
			// op_set_common_data(int _key, int _set_value, bool _is_value)
			if(p->type == 15)
			{
				label9->Text="el_id";
				textBox2->Text=((int)p->parameters[0]).ToString();
				label10->Text="amoun";
				textBox3->Text=((int)p->parameters[1]).ToString();
				label11->Text="distance";
				textBox4->Text=((int)p->parameters[2]).ToString();
				label12->Text="duration";
				textBox5->Text=((int)p->parameters[3]).ToString();
				label13->Text="name";
				textBox6->Text=(Encoding::Unicode)->GetString((array<unsigned char>^)p->parameters[4])->Replace("\0", "");
				label14->Text="path";
				textBox7->Text=((int)p->parameters[5]).ToString();
				label15->Text="unk";
				textBox8->Text=((int)p->parameters[6]).ToString();
				textBox2->Enabled= true;
				textBox3->Enabled= true;
				textBox4->Enabled= true;
				textBox5->Enabled= true;
				textBox6->Enabled= true;
				textBox7->Enabled= true;
			}
			// op_add_common_data(int _key, int _add_value)
			if(p->type == 16)
			{
				label9->Text="key";
				textBox2->Text=((int)p->parameters[0]).ToString();
				textBox2->Enabled= true;
			}
			// op_summon_monster(int _mob_id, int _count, int _target_distance, int _remain_time, char _die_with_who, int _path_id)
			if(p->type == 17)
			{
				label9->Text="unk";
				textBox2->Text=((int)p->parameters[0]).ToString();
				label10->Text="unk";
				textBox3->Text=((int)p->parameters[1]).ToString();
				textBox2->Enabled= true;
				textBox3->Enabled= true;
			}
			// op_change_path(int _world_tag, int _global_path_id, int _path_type, char _speed_flag)
			if(p->type == 18)
			{
				//expression = "Change_Path(";
				//expression += ")";
			}
			// op_play_action(char _action_name[128], int _play_times, int _action_last_time, int _interval_time)
			if(p->type == 19)
			{
				label9->Text="name";
				textBox2->Text=(Encoding::GetEncoding("GBK"))->GetString((array<unsigned char>^)p->parameters[0])->Replace("\0", "");
				label10->Text="times";
				textBox3->Text=((int)p->parameters[1]).ToString();
				label11->Text="ac_last_tim";
				textBox2->Text=((int)p->parameters[2]).ToString();
				label12->Text="interval";
				textBox4->Text=((int)p->parameters[3]).ToString();
				textBox2->Enabled= true;
				textBox3->Enabled= true;
				textBox4->Enabled= true;
			}
			if(p->type == 20)
			{
				//expression = "Unknown1(";
			}
			if(p->type == 21)
			{
				//expression = "Unknown2(";
			}
			if(p->type == 22)
			{
				label9->Text="unk";
				textBox2->Text=((int)p->parameters[0]).ToString();
				label10->Text="unk";
				textBox3->Text=((int)p->parameters[1]).ToString();
				textBox2->Enabled= true;
				textBox3->Enabled= true;
			}
			if(p->type == 23)
			{
				label9->Text="unk";
				textBox2->Text=((int)p->parameters[0]).ToString();
				label10->Text="unk";
				textBox3->Text=((int)p->parameters[1]).ToString();
				textBox2->Enabled= true;
				textBox3->Enabled= true;
			}
			if(p->type == 24)
			{
				//expression = "Unknown5(";
			}
			if(p->type == 25)
			{
				label9->Text="unk";
				textBox2->Text=((int)p->parameters[0]).ToString();
				label10->Text="unk";
				textBox3->Text=((int)p->parameters[1]).ToString();
				label11->Text="unk";
				textBox4->Text=((int)p->parameters[2]).ToString();
				label12->Text="unk";
				textBox5->Text=((__int16)p->parameters[3]).ToString();
				label13->Text="unk";
				textBox6->Text=((__int16)p->parameters[4]).ToString();
				label14->Text="unk";
				textBox7->Text=((int)p->parameters[5]).ToString();
				textBox2->Enabled= true;
				textBox3->Enabled= true;
				textBox4->Enabled= true;
				textBox5->Enabled= true;
				textBox6->Enabled= true;
				textBox7->Enabled= true;
			}
			if(p->type == 26)
			{
				label9->Text="unk";
				textBox2->Text=((int)p->parameters[0]).ToString();
				label10->Text="unk";
				textBox3->Text=((int)p->parameters[1]).ToString();
				label11->Text="unk";
				textBox4->Text=((int)p->parameters[2]).ToString();
				textBox2->Enabled= true;
				textBox3->Enabled= true;
				textBox4->Enabled= true;
			}
			if(p->type == 27)
			{
				//expression = "Unknown8(";
			}
			if(p->type == 28)
			{
				label9->Text="unk";
				textBox2->Text=((int)p->parameters[0]).ToString();
				textBox2->Enabled= true;
			}
			if(p->type == 29)
			{
				label9->Text="unk";
				textBox2->Text=((int)p->parameters[0]).ToString();
				textBox2->Enabled= true;
			}
			if(p->type == 30)
			{
				//expression = "Unknown11(";

			}
		}

		array<Object^>^ read_target_parameters(int type, BinaryReader^ br)
		{
			if(type == 0)
			{
				return gcnew array<Object^>{};
			}
			if(type == 1)
			{
				return gcnew array<Object^>{};
			}
			if(type == 2)
			{
				return gcnew array<Object^>{};
			}
			if(type == 3)
			{
				return gcnew array<Object^>{};
			}
			if(type == 4)
			{
				return gcnew array<Object^>{};
			}
			if(type == 5)
			{
				return gcnew array<Object^>{};
			}
			if(type == 6)
			{
				return gcnew array<Object^>{br->ReadInt32()};
			}
			if(type == 7)
			{
				return gcnew array<Object^>{};
			}
			//comper
			if(type == 11)
			{
				return gcnew array<Object^>{br->ReadInt16()};
			}
			return gcnew array<Object^>(0);
		}
		void write_target_parameters(int type, array<Object^>^ Parameters, BinaryWriter^ bw)
		{
			//if(type == 6)
			//{
			//	bw->Write((int)Parameters[0]);
			//}
		}
		array<Object^>^ build_parameters(int type)
		{
			if(type == 0)
			{
				return gcnew array<Object^>{1};
			}
			if(type == 1)
			{
				return gcnew array<Object^>{1,2};
			}
			if(type == 2)
			{
				return gcnew array<Object^>{36, string_to_bytes("Broadcast message"+"\0", "Unicode", 36)};
			}
			if(type == 3)
			{
				return gcnew array<Object^>{};
			}
			if(type == 4)
			{
				return gcnew array<Object^>{1};
			}
			if(type == 5)
			{
				return gcnew array<Object^>{1};
			}
			if(type == 6)
			{
				return gcnew array<Object^>{1};
			}
			if(type == 7)
			{
				return gcnew array<Object^>{1, 2, 3};
			}
			if(type == 8)
			{
				return gcnew array<Object^>{1};
			}
			if(type == 9)
			{
				return gcnew array<Object^>{};
			}
			if(type == 10)
			{
				return gcnew array<Object^>{};
			}
			if(type == 11)
			{
				return gcnew array<Object^>{};
			}
			if(type == 12)
			{
				return gcnew array<Object^>{};
			}
			if(type == 13)
			{
				return gcnew array<Object^>{};
			}
			if(type == 14)
			{
				return gcnew array<Object^>{1, ((__int16)2), ((__int16)3)};
			}
			if(type == 15)
			{
				return gcnew array<Object^>{1, 2, 3, 4, string_to_bytes("Mob name\0", "Unicode", 32), 6, 7};
			}
			if(type == 16)
			{
				return gcnew array<Object^>{1};
			}
			if(type == 17)
			{
				return gcnew array<Object^>{1, 2};
			}
			if(type == 18)
			{
				return gcnew array<Object^>{};
			}
			if(type == 19)
			{
				return gcnew array<Object^>{string_to_bytes("String\0", "Unicode", 128), 2, 3, 4};
			}
			if(type == 20)
			{
				return gcnew array<Object^>{};
			}
			if(type == 21)
			{
				return gcnew array<Object^>{};
			}
			if(type == 22)
			{
				return gcnew array<Object^>{1, 2};
			}
			if(type == 23)
			{
				return gcnew array<Object^>{1, 2};
			}
			if(type == 24)
			{
				return gcnew array<Object^>{};
			}
			if(type == 25)
			{
				return gcnew array<Object^>{1, 2, 3, ((__int16)4), ((__int16)5), 6};
			}
			if(type == 26)
			{
				return gcnew array<Object^>{1, 2, 3};
			}
			if(type == 27)
			{
				return gcnew array<Object^>{};
			}
			if(type == 28)
			{
				return gcnew array<Object^>{1};
			}
			if(type == 29)
			{
				return gcnew array<Object^>{1};
			}
			if(type == 30)
			{
				return gcnew array<Object^>{};
			}
			if(type > 30)
			{
				//MessageBox::Show("Unsupported procedure detected: Type " + type);
			}
			return gcnew array<Object^>(0);
		}
		array<Object^>^ read_parameters(int type, BinaryReader^ br)
		{
			if(type == 0)
			{
				return gcnew array<Object^>{br->ReadInt32()};
			}
			if(type == 1)
			{
				return gcnew array<Object^>{br->ReadInt32(), br->ReadInt32()};
			}
			if(type == 2)
			{
				int count = br->ReadInt32();
				return gcnew array<Object^>{count, br->ReadBytes(count)};
			}
			if(type == 3)
			{
				return gcnew array<Object^>{};
			}
			if(type == 4)
			{
				return gcnew array<Object^>{br->ReadInt32()};
			}
			if(type == 5)
			{
				return gcnew array<Object^>{br->ReadInt32()};
			}
			if(type == 6)
			{
				return gcnew array<Object^>{br->ReadInt32()};
			}
			if(type == 7)
			{
				return gcnew array<Object^>{br->ReadInt32(), br->ReadInt32(), br->ReadInt32()};
			}
			if(type == 8)
			{
				return gcnew array<Object^>{br->ReadInt32()};
			}
			if(type == 9)
			{
				return gcnew array<Object^>{};
			}
			if(type == 10)
			{
				return gcnew array<Object^>{};
			}
			if(type == 11)
			{
				return gcnew array<Object^>{};
			}
			if(type == 12)
			{
				return gcnew array<Object^>{};
			}
			if(type == 13)
			{
				return gcnew array<Object^>{};
			}
			if(type == 14)
			{
				return gcnew array<Object^>{br->ReadInt32(), br->ReadInt16(), br->ReadInt16()};
			}
			if(type == 15)
			{
				return gcnew array<Object^>{br->ReadInt32(), br->ReadInt32(), br->ReadInt32(),br->ReadInt32(),br->ReadBytes(32),br->ReadInt32(),br->ReadInt32()};
			}
			if(type == 16)
			{
				return gcnew array<Object^>{br->ReadInt32()};
			}
			if(type == 17)
			{
				return gcnew array<Object^>{br->ReadInt32(), br->ReadInt32()};
			}
			if(type == 18)
			{
				return gcnew array<Object^>{}; //br->ReadInt32(), br->ReadInt32(), br->ReadInt32()
			}
			if(type == 19)
			{
				return gcnew array<Object^>{br->ReadBytes(128), br->ReadInt32(), br->ReadInt32(), br->ReadInt32()};
			}
			if(type == 20)
			{
				return gcnew array<Object^>{};
			}
			if(type == 21)
			{
				return gcnew array<Object^>{};
			}
			if(type == 22)
			{
				return gcnew array<Object^>{br->ReadInt32(), br->ReadInt32()};
			}
			if(type == 23)
			{
				return gcnew array<Object^>{br->ReadInt32(), br->ReadInt32()};
			}
			if(type == 24)
			{
				return gcnew array<Object^>{};
			}
			if(type == 25)
			{
				return gcnew array<Object^>{br->ReadInt32(), br->ReadInt32(),br->ReadInt32(), br->ReadInt16(),br->ReadInt16(),br->ReadInt32()};
			}
			if(type == 26)
			{
				return gcnew array<Object^>{br->ReadInt32(), br->ReadInt32(),br->ReadInt32()};
			}
			if(type == 27)
			{
				return gcnew array<Object^>{br->ReadInt32()};
			}
			if(type == 28)
			{
				return gcnew array<Object^>{br->ReadInt32()};
			}
			if(type == 29)
			{
				return gcnew array<Object^>{br->ReadInt32()};
			}
			if(type > 29)
			{
				//MessageBox::Show("Unsupported procedure detected: Type " + type);
			}
			return gcnew array<Object^>(0);
		}
		void write_parameters(int type, array<Object^>^ Parameters, BinaryWriter^ bw)
		{
			if(type == 0)
			{
				bw->Write((int)Parameters[0]);
			}
			if(type == 1)
			{
				bw->Write((int)Parameters[0]);
				bw->Write((int)Parameters[1]);
			}
			if(type == 2)
			{
				int count = (int)Parameters[0];
				bw->Write(count);
				bw->Write((array<unsigned char>^)Parameters[1]);
			}
			if(type == 4)
			{
				bw->Write((int)Parameters[0]);
			}
			if(type == 5)
			{
				bw->Write((int)Parameters[0]);
			}
			if(type == 6)
			{
				bw->Write((int)Parameters[0]);
			}
			if(type == 7)
			{
				bw->Write((int)Parameters[0]);
				bw->Write((int)Parameters[1]);
				bw->Write((int)Parameters[2]);
			}
			if(type == 8)
			{
				bw->Write((int)Parameters[0]);
			}
			if(type == 14)
			{
				bw->Write((int)Parameters[0]);
				bw->Write((__int16)Parameters[1]);
				bw->Write((__int16)Parameters[2]);
			}
			if(type == 15)
			{
				bw->Write((int)Parameters[0]);
				bw->Write((int)Parameters[1]);
				bw->Write((int)Parameters[2]);
				bw->Write((int)Parameters[3]);
				bw->Write((array<unsigned char>^)Parameters[4]);
				//bw->Write((int)Parameters[4]);
				bw->Write((int)Parameters[5]);
				bw->Write((int)Parameters[6]);
			}
			if(type == 16)
			{
				bw->Write((int)Parameters[0]);
				//bw->Write((int)Parameters[1]);
			}
			if(type == 17)
			{
				bw->Write((int)Parameters[0]);
				bw->Write((int)Parameters[1]);
				//bw->Write((int)Parameters[2]);
				//bw->Write((int)Parameters[3]);
				//bw->Write((int)Parameters[4]);
				//bw->Write((int)Parameters[5]);
			}
			//if(type == 18)
			//{
			//	bw->Write((int)Parameters[0]);
			//	bw->Write((int)Parameters[1]);
			//	bw->Write((int)Parameters[2]);
			//	bw->Write((int)Parameters[3]);
			//}
			if(type == 19)
			{
				bw->Write((array<unsigned char>^)Parameters[0]);
				bw->Write((int)Parameters[1]);
				bw->Write((int)Parameters[2]);
				bw->Write((int)Parameters[3]);
			}
			if(type == 22)
			{
				bw->Write((int)Parameters[0]);
				bw->Write((int)Parameters[1]);
			}
			if(type == 23)
			{
				bw->Write((int)Parameters[0]);
				bw->Write((int)Parameters[1]);
			}
			if(type == 25)
			{
				bw->Write((int)Parameters[0]);
				bw->Write((int)Parameters[1]);
				bw->Write((int)Parameters[2]);
				bw->Write((__int16)Parameters[3]);
				bw->Write((__int16)Parameters[4]);
				bw->Write((int)Parameters[5]);
			}
			if(type == 26)
			{
				bw->Write((int)Parameters[0]);
				bw->Write((int)Parameters[1]);
				bw->Write((int)Parameters[2]);
			}
			if(type == 27)
			{
				bw->Write((int)Parameters[0]);
			}
			if(type == 28)
			{
				bw->Write((int)Parameters[0]);
			}
			if(type == 29)
			{
				bw->Write((int)Parameters[0]);
			}
		}

		Condition^ load_condition(BinaryReader^ br)
		{
			Condition^ c = gcnew Condition();

			c->operator_id = br->ReadInt32();
			c->argument_bytes = br->ReadInt32();
			
			c->value = br->ReadBytes(c->argument_bytes);
			c->condition_type = br->ReadInt32(); // 1 || 2 || 3
			if(c->condition_type == 1)
			{
				c->condition_left = load_condition(br);
				c->subnode_2 = br->ReadInt32(); // always 2
				c->condition_right = load_condition(br);
				c->subnode_3 = br->ReadInt32(); // always 4
				//if(c->subnode_2!=2) MessageBox::Show("IMPORT!\n\n" +c->subnode_2 +";"+c->subnode_3);
			}

			if(c->condition_type == 2)
			{
				c->condition_right = load_condition(br);
				c->subnode_2 = br->ReadInt32(); // always 4
			}

			return c;
		}

		void save_condition(Condition^ c, BinaryWriter^ bw)
		{
			bw->Write(c->operator_id);
			bw->Write(c->argument_bytes);
			//if(c->argument_bytes == 4)
			//{
				bw->Write(c->value);
			//}

			bw->Write(c->condition_type);
			//if(c->operator_id==10) MessageBox::Show("EXPORT!\n\n" + c->condition_type +";"+c->argument_bytes);
			if(c->condition_type == 1)
			{
				save_condition(c->condition_left, bw);
				bw->Write(c->subnode_2);
				//if(c->subnode_2 == 2)
				//{
				save_condition(c->condition_right, bw);
				bw->Write(c->subnode_3);
				//}
			}

			if(c->condition_type == 2)
			{
				save_condition(c->condition_right, bw);
				bw->Write(c->subnode_2);
			}
		}

		private: System::Void click_load(System::Object^  sender, System::EventArgs^  e)
		{
			OpenFileDialog^ load = gcnew OpenFileDialog();
			load->Filter = "AI Policy (*.data)|*.data|All Files (*.*)|*.*";
			if(load->ShowDialog() == Windows::Forms::DialogResult::OK && File::Exists(load->FileName))
			{
				try
				{
					Cursor = Windows::Forms::Cursors::WaitCursor;

					listBox_ActionController->Items->Clear();
					listBox_ActionSet->Items->Clear();
					textBox_Condition->Clear();
					listBox_Procedures->Items->Clear();

					FileStream^ fr = File::OpenRead(load->FileName);
					BinaryReader^ br = gcnew BinaryReader(fr);

					AI = gcnew AIPolicy();

					AI->signature = br->ReadInt32();
					AI->action_controllers_count = br->ReadInt32();
					AI->action_controllers = gcnew array<ActionController^>(AI->action_controllers_count);
					for(int ac=0; ac<AI->action_controllers->Length; ac++)
					{
						
						AI->action_controllers[ac] = gcnew ActionController();
						AI->action_controllers[ac]->signature = br->ReadInt32();
						AI->action_controllers[ac]->id = br->ReadInt32();
						AI->action_controllers[ac]->action_sets_count = br->ReadInt32();
						AI->action_controllers[ac]->action_sets = gcnew array<ActionSet^>(AI->action_controllers[ac]->action_sets_count);
						listBox_ActionController->Items->Add(AI->action_controllers[ac]->id.ToString());
						for(int as=0; as<AI->action_controllers[ac]->action_sets->Length; as++)
						{
							AI->action_controllers[ac]->action_sets[as] = gcnew ActionSet();
							AI->action_controllers[ac]->action_sets[as]->version = br->ReadInt32();
							//if(ac>2680) MessageBox::Show("comper!\n\n" + AI->action_controllers[ac]->action_sets[as]->version);
							AI->action_controllers[ac]->action_sets[as]->id = br->ReadInt32();
							AI->action_controllers[ac]->action_sets[as]->flags = br->ReadBytes(3);
							AI->action_controllers[ac]->action_sets[as]->name = br->ReadBytes(128);
							AI->action_controllers[ac]->action_sets[as]->conditions = load_condition(br);
							//if(AI->action_controllers[ac]->action_sets[as]->conditions->condition_type==2&&AI->action_controllers[ac]->action_sets[as]->conditions->condition_right->condition_type!=3) MessageBox::Show("IMPORT!\n\n" + AI->action_controllers[ac]->id);
							AI->action_controllers[ac]->action_sets[as]->procedures_count = br->ReadInt32();
							AI->action_controllers[ac]->action_sets[as]->procedures = gcnew array<Procedure^>(AI->action_controllers[ac]->action_sets[as]->procedures_count);
							for(int p=0; p<AI->action_controllers[ac]->action_sets[as]->procedures->Length; p++)
							{
								AI->action_controllers[ac]->action_sets[as]->procedures[p] = gcnew Procedure();
								AI->action_controllers[ac]->action_sets[as]->procedures[p]->type = br->ReadInt32();
								AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters = read_parameters(AI->action_controllers[ac]->action_sets[as]->procedures[p]->type, br);
								AI->action_controllers[ac]->action_sets[as]->procedures[p]->target_type = br->ReadInt32();
								if(AI->action_controllers[ac]->action_sets[as]->procedures[p]->target_type == 11)
								{
									//MessageBox::Show("proc" + p +":"+AI->action_controllers[ac]->action_sets[as]->procedures[p]->type + "t:"+AI->action_controllers[ac]->action_sets[as]->procedures[p]->target_type);
									//AI->action_controllers[ac]->action_sets[as]->procedures[p]->target_parameters = read_target_parameters(AI->action_controllers[ac]->action_sets[as]->procedures[p]->target_type, br);
									//br->ReadBytes(21);
								}
								//
							}
						}
					}

					br->Close();
					fr->Close();

					Cursor = Windows::Forms::Cursors::Default;
				}
				catch(Exception^ e)
				{
					Cursor = Windows::Forms::Cursors::Default;
					MessageBox::Show("IMPORT ERROR!\n\n" + e->Message);
				}
			}
		}

		private: System::Void click_export(System::Object^  sender, System::EventArgs^  e)
		{
			SaveFileDialog^ save = gcnew SaveFileDialog();
			save->Filter = "AI Policy (*.data)|*.data|All Files (*.*)|*.*";
			if(AI && save->ShowDialog() == Windows::Forms::DialogResult::OK && save->FileName != "")
			{
				Cursor = Windows::Forms::Cursors::WaitCursor;

				//AI = convert(AI);

				FileStream^ fw = gcnew FileStream(save->FileName, FileMode::Create, FileAccess::Write);
				BinaryWriter^ bw = gcnew BinaryWriter(fw);

				bw->Write(AI->signature);
				bw->Write(AI->action_controllers_count);
				for(int ac=0; ac<AI->action_controllers->Length; ac++)
				{
					bw->Write(AI->action_controllers[ac]->signature);
					bw->Write(AI->action_controllers[ac]->id);
					bw->Write(AI->action_controllers[ac]->action_sets_count);
					for(int as=0; as<AI->action_controllers[ac]->action_sets->Length; as++)
					{
						bw->Write(AI->action_controllers[ac]->action_sets[as]->version);
						bw->Write(AI->action_controllers[ac]->action_sets[as]->id);
						bw->Write(AI->action_controllers[ac]->action_sets[as]->flags);
						bw->Write(AI->action_controllers[ac]->action_sets[as]->name);
						// Write conditions
						save_condition(AI->action_controllers[ac]->action_sets[as]->conditions, bw);
						bw->Write(AI->action_controllers[ac]->action_sets[as]->procedures_count);
						for(int p=0; p<AI->action_controllers[ac]->action_sets[as]->procedures->Length; p++)
						{
							bw->Write(AI->action_controllers[ac]->action_sets[as]->procedures[p]->type);
							write_parameters(AI->action_controllers[ac]->action_sets[as]->procedures[p]->type, AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters, bw);
							bw->Write(AI->action_controllers[ac]->action_sets[as]->procedures[p]->target_type);
							//if(AI->action_controllers[ac]->action_sets[as]->procedures[p]->target_type == 6)
							//{
							//	write_target_parameters(AI->action_controllers[ac]->action_sets[as]->procedures[p]->target_type, AI->action_controllers[ac]->action_sets[as]->procedures[p]->target_parameters, bw);
							//}
						}
					}
				}
				 MessageBox::Show("File saved!");
				bw->Close();
				fw->Close();

				Cursor = Windows::Forms::Cursors::Default;
			}
		}

		/*
			Rebuild the condition tree
			If conditions are based on property ID's > 8, then this condition will be set to NULL
		*/
		Condition^ fix_conditions(Condition^ c)
		{
			if(c->operator_id > 8)
			{
				// try to replace operator id with a valid condition_(chain)

				// remove condition
				return nullptr;
			}
			else
			{
				if(c->condition_type == 1)
				{
					c->condition_left = fix_conditions(c->condition_left);
					c->condition_right = fix_conditions(c->condition_right);

					if(c->condition_left && c->condition_right)
					{
						// everything is OK
						return c;
					}
					if(!c->condition_left && c->condition_right)
					{
						// left condition is broken, no need to operate between both conditions
						// right condition replaces the current operator condition i.e. AND
						return c->condition_right;
					}
					if(c->condition_left && !c->condition_right)
					{
						// right condition is broken, no need to operate between both conditions
						// left condition replaces the current operator condition i.e. AND
						return c->condition_left;
					}
					if(!c->condition_left && !c->condition_right)
					{
						// both conditions for this operator condition are broken
						// completely drop this condition
						return nullptr;
					}
				}
				if(c->condition_type == 2)
				{
					c->condition_right = fix_conditions(c->condition_right);

					if(c->condition_right)
					{
						return c;
					}
					else
					{
						return nullptr;
					}
				}
				//if(c->condition_type > 2)
				return c;
			}
		}

		/*
			Rebuild procedure list by removing all procedure types >= 15,
			or remove procedures that calls monster skills with id >= 887
		*/
		array<Procedure^>^ fix_procedures(array<Procedure^>^ p)
		{
			int count = 0;
			for(int i=0; i<p->Length; i++)
			{
				// count supported procedures
				if(p[i]->type < 18 && p[i]->type != 15 && p[i]->type != 16)
				{
					count++;
				}
			}

			array<Procedure^>^ result = gcnew array<Procedure^>(count);
			count = 0;

			for(int i=0; i<p->Length; i++)
			{
				// only keep supported procedures 
				if(p[i]->type < 18 && p[i]->type != 15 && p[i]->type != 16)
				{
					result[count] = gcnew Procedure();
					result[count]->type = p[i]->type;
					result[count]->parameters = p[i]->parameters;
					// replace skill if possible
					if(p[i]->type == 1 && skillReplace->ContainsKey((int)p[i]->parameters[0]))
					{
						result[count]->parameters[0] = (int)skillReplace[(int)p[i]->parameters[0]];
					}

					if(p[i]->type == 2 && ((array<unsigned char>^)result[count]->parameters[1])[0] == '$')
					{
						// remove whitespace after macro
						if(((array<unsigned char>^)result[count]->parameters[1])[4] == ' ')
						{
							array<unsigned char>^ _old = ((array<unsigned char>^)result[count]->parameters[1]);
							array<unsigned char>^ _new = gcnew array<unsigned char>(_old->Length-2);

							int n=0;
							for(int i=0; i<_new->Length; i++)
							{
								if(n == 4)
								{
									n = 6;
								}
								_new[i] = _old[n];
								n++;
							}

							result[count]->parameters[1] = _new;
							result[count]->parameters[0] = _new->Length;
						}

						// replace $S macro (no-name, color?) with $B macro (no-name, gold)
						if(((array<unsigned char>^)result[count]->parameters[1])[2] == 'S')
						{
							((array<unsigned char>^)result[count]->parameters[1])[2] = 'B';
						}

						// replace $I macro (no-name, red?) with $B macro (no-name, gold)
						if(((array<unsigned char>^)result[count]->parameters[1])[2] == 'I')
						{
							((array<unsigned char>^)result[count]->parameters[1])[2] = 'B';
						}
					}
					result[count]->target_type = p[i]->target_type;
					result[count]->target_parameters = p[i]->target_parameters;

					// replace player_aimed_npc_spawn with a simple npc_generator
					if(p[i]->type == 17)
					{
						result[count]->type = 14;
						// using i as unifier to prevent multiple npc_generator() with same id's in the same action set
						int creature_builder_id = (GUID*1000000 + (int)p[i]->parameters[0]);
						GUID++;
						int options = 0; // even(0): start, odd(1): stop
						result[count]->parameters = gcnew array<Object^>{creature_builder_id, options};
						result[count]->target_type = 0;
						result[count]->target_parameters = gcnew array<Object^>{};
					}

					count++;
				}
			}

			// if result is length 0 we have to add a dummy procedure, or action set will crash map
			if(result->Length < 1)
			{
				result = gcnew array<Procedure^>(1);
				result[0] = gcnew Procedure();
				result[0]->type = 2;
				result[0]->parameters = gcnew array<Object^>{6, Encoding::Unicode->GetBytes("...")};
				result[0]->target_type = 0;
				result[0]->target_parameters = gcnew array<Object^>{};
			}

			return result;
		}
		String^ bytes_to_string(array<unsigned char>^ value, String^ code)
		{
			System::Text::Encoding^ enc = System::Text::Encoding::GetEncoding(code);
			return enc->GetString(value);
		}
		array<unsigned char>^ string_to_bytes(String^ value, String^ code, int bytes)
		{
			System::Text::Encoding^ enc = System::Text::Encoding::GetEncoding(code);
			array<unsigned char>^ target = gcnew array<unsigned char>(bytes);
			array<unsigned char>^ source = enc->GetBytes(value);
			if(target->Length > source->Length)
			{
				Array::Copy(source, target, source->Length);
			}
			else
			{
				Array::Copy(source, target, target->Length);
			}
			return target;
		}
		/*
			try to convert any AI structure to a 1.3.6 compatible server version
			+ replace signature and versions for file, collections and sets
			+ remove unsupported conditions (condition->property > 8) and rebuild condition tree
			+ remove unsupported procedures (procedure->id > 14)
			+ remove procedures that are using monster skills with id > 887
		*/
		AIPolicy^ convert(AIPolicy^ source_ai)
		{
			// a countr for creating unique creature builder id's
			// when replacing player_aimed_npc_spawn() procedure with npc_generate() procedure
			GUID = 1;

			AIPolicy^ result = gcnew AIPolicy();
			result->signature = (int)0; // signature = 0 for <= 1.3.6
			result->action_controllers_count = source_ai->action_controllers_count;
			result->action_controllers = gcnew array<ActionController^>(result->action_controllers_count);

			for(int ac=0; ac<result->action_controllers->Length; ac++)
			{
				result->action_controllers[ac] = gcnew ActionController();
				result->action_controllers[ac]->signature = (int)0; // signature = 0 for <= 1.3.6
				result->action_controllers[ac]->id = source_ai->action_controllers[ac]->id;
				result->action_controllers[ac]->action_sets_count = source_ai->action_controllers[ac]->action_sets_count;
				result->action_controllers[ac]->action_sets = gcnew array<ActionSet^>(result->action_controllers[ac]->action_sets_count);

				for(int as=0; as<result->action_controllers[ac]->action_sets->Length; as++)
				{
					result->action_controllers[ac]->action_sets[as] = gcnew ActionSet();
					result->action_controllers[ac]->action_sets[as]->version = (int)1; // signature = 1 for <= 1.3.6
					result->action_controllers[ac]->action_sets[as]->id = source_ai->action_controllers[ac]->action_sets[as]->id;
					result->action_controllers[ac]->action_sets[as]->flags = source_ai->action_controllers[ac]->action_sets[as]->flags; // 3 Bytes
					result->action_controllers[ac]->action_sets[as]->name = source_ai->action_controllers[ac]->action_sets[as]->name; // 128 Bytes
					// convert conditions
					result->action_controllers[ac]->action_sets[as]->conditions = fix_conditions(source_ai->action_controllers[ac]->action_sets[as]->conditions);
					// convert procedures
					result->action_controllers[ac]->action_sets[as]->procedures = fix_procedures(source_ai->action_controllers[ac]->action_sets[as]->procedures);
					result->action_controllers[ac]->action_sets[as]->procedures_count = result->action_controllers[ac]->action_sets[as]->procedures->Length;

					// check for nullptr conditions after conversion
					// because it could happen that condition only consists
					// of properties > 1.3.6, this will cause in complete useless of condition tree 1.3.6
					if(!result->action_controllers[ac]->action_sets[as]->conditions)
					{
						// Removing an actionset can leads to dead links, if a link from a procedure is pointing to this actionset id
						// instead of removing this actionset, we create a default condition that is common false,
						// but will be executed when linked by an execute_actionset() procedure
						// for this behaviour we are using randomize(1.00)
						result->action_controllers[ac]->action_sets[as]->conditions = gcnew Condition();
						result->action_controllers[ac]->action_sets[as]->conditions->operator_id = 3;
						result->action_controllers[ac]->action_sets[as]->conditions->argument_bytes = 4;
						if(blockDisable->Contains(source_ai->action_controllers[ac]->id + ":" + source_ai->action_controllers[ac]->action_sets[as]->id))
						{
							result->action_controllers[ac]->action_sets[as]->conditions->value = gcnew array<unsigned char>{0, 0, 0, 0};
						}
						else
						{
							result->action_controllers[ac]->action_sets[as]->conditions->value = gcnew array<unsigned char>{0, 0, 128, 63};
						}
						result->action_controllers[ac]->action_sets[as]->conditions->subnode_2 = 0;
						result->action_controllers[ac]->action_sets[as]->conditions->subnode_3 = 0;
						result->action_controllers[ac]->action_sets[as]->conditions->condition_type = 3;
					}
				}
			}

			return result;
		}
		private: System::Void change_Procedure_target(System::Object^  sender, System::EventArgs^  e)
		{
			if(AI && listBox_Procedures->SelectedIndex > -1)
			{
				int ac = listBox_ActionController->SelectedIndex;
				int as = listBox_ActionSet->SelectedIndex;
				int p = listBox_Procedures->SelectedIndex;
				if(AI->action_controllers[ac]->action_sets[as]->procedures[p]->target_type != comboBox2->SelectedIndex){
					AI->action_controllers[ac]->action_sets[as]->procedures[p]->target_type=comboBox2->SelectedIndex;

					listBox_Procedures->Items->Clear();
					for(int i=0; i<AI->action_controllers[ac]->action_sets[as]->procedures->Length; i++)
					{
						listBox_Procedures->Items->Add("[" + i.ToString() + "] " + procedure_expression(AI->action_controllers[ac]->action_sets[as]->procedures[i]));
					}
				}
			}
		}
		private: System::Void change_Procedure_type(System::Object^  sender, System::EventArgs^  e)
		{
			if(AI && listBox_Procedures->SelectedIndex > -1)
			{
				int ac = listBox_ActionController->SelectedIndex;
				int as = listBox_ActionSet->SelectedIndex;
				int p = listBox_Procedures->SelectedIndex;
				if(AI->action_controllers[ac]->action_sets[as]->procedures[p]->type != comboBox1->SelectedIndex){
					
					//delete AI->action_controllers[ac]->action_sets[as]->procedures[p];
					//AI->action_controllers[ac]->action_sets[as]->procedures[p]=gcnew Procedure();
					array <Procedure^>^ temp = gcnew array<Procedure^>(AI->action_controllers[ac]->action_sets[as]->procedures_count);
					Array::Copy(AI->action_controllers[ac]->action_sets[as]->procedures, 0, temp, 0, p);
					temp[p]= gcnew Procedure();
					temp[p]->type=comboBox1->SelectedIndex;
					temp[p]->parameters = build_parameters(temp[p]->type);
					temp[p]->target_type = comboBox2->SelectedIndex;
					temp[p]->target_parameters = gcnew array<Object^>{};
					//MessageBox::Show("Th5."+AI->action_controllers[ac]->action_sets[as]->procedures_count);
					Array::Copy(AI->action_controllers[ac]->action_sets[as]->procedures, p+1, temp, p+1, AI->action_controllers[ac]->action_sets[as]->procedures_count - p - 1);
					AI->action_controllers[ac]->action_sets[as]->procedures = temp;
					listBox_Procedures->Items->Clear();
					for(int i=0; i<AI->action_controllers[ac]->action_sets[as]->procedures->Length; i++)
					{
						listBox_Procedures->Items->Add("[" + i.ToString() + "] " + procedure_expression(AI->action_controllers[ac]->action_sets[as]->procedures[i]));
					}
				}
			}
		}
		private: System::Void change_Procedures(System::Object^  sender, System::EventArgs^  e)
		{
			if(AI && listBox_Procedures->SelectedIndex > -1)
			{
				int ac = listBox_ActionController->SelectedIndex;
				int as = listBox_ActionSet->SelectedIndex;
				int p = listBox_Procedures->SelectedIndex;
				textBox2->Text="";
				textBox3->Text="";
				textBox4->Text="";
				textBox5->Text="";
				textBox6->Text="";
				textBox7->Text="";
				textBox8->Text="";
				textBox2->Enabled= false;
				textBox3->Enabled= false;
				textBox4->Enabled= false;
				textBox5->Enabled= false;
				textBox6->Enabled= false;
				textBox7->Enabled= false;
				textBox8->Enabled= false;
				label9->Text="1";
				label10->Text="2";
				label11->Text="3";
				label12->Text="4";
				label13->Text="5";
				label14->Text="6";
				label15->Text="7";
				procedure_show(AI->action_controllers[ac]->action_sets[as]->procedures[p]);
				comboBox2->SelectedIndex = AI->action_controllers[ac]->action_sets[as]->procedures[p]->target_type;
				comboBox1->SelectedIndex = AI->action_controllers[ac]->action_sets[as]->procedures[p]->type;
			}
		}
		private: System::Void change_ActionController(System::Object^  sender, System::EventArgs^  e)
		{
			if(AI && listBox_ActionController->SelectedIndex > -1)
			{
				int ac = listBox_ActionController->SelectedIndex;

				listBox_ActionSet->Items->Clear();
				for(int i=0; i<AI->action_controllers[ac]->action_sets->Length; i++)
				{
					listBox_ActionSet->Items->Add("[" + AI->action_controllers[ac]->action_sets[i]->id.ToString() + "] " + AI->action_controllers[ac]->action_sets[i]->Name);
				}
				textBox_Condition->Clear();
				listBox_Procedures->Items->Clear();
			}
		}
		private: System::Void change_ActionSet(System::Object^  sender, System::EventArgs^  e)
		{
			if(AI && listBox_ActionController->SelectedIndex > -1 && listBox_ActionSet->SelectedIndex > -1)
			{
				int ac = listBox_ActionController->SelectedIndex;
				int as = listBox_ActionSet->SelectedIndex;

				textBox_Condition->Text = "IF{ " + condition_expression(AI->action_controllers[ac]->action_sets[as]->conditions) + " }";
				
				listBox_Procedures->Items->Clear();
				for(int i=0; i<AI->action_controllers[ac]->action_sets[as]->procedures->Length; i++)
				{
					listBox_Procedures->Items->Add("[" + i.ToString() + "] " + procedure_expression(AI->action_controllers[ac]->action_sets[as]->procedures[i]));
				}
	
				textBox1->Text = AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->Name;// listBox_ActionSet->Items[listBox_ActionSet->SelectedIndex]->ToString();
				comboBox5->Enabled= false;
				comboBox6->Enabled= false;
				comboBox4->Enabled= false;
				comboBox7->Enabled= false;
				comboBox8->Enabled= false;
				textBox9->Enabled=false;
				textBox10->Enabled=false;
				textBox11->Enabled=false;
				textBox9->Text="";
				textBox10->Text="";
				textBox11->Text="";
				comboBox5->SelectedIndex=-1;
				comboBox6->SelectedIndex=-1;
				comboBox7->SelectedIndex=-1;
				comboBox8->SelectedIndex=-1;
				comboBox4->SelectedIndex = AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->conditions->condition_type-1;
				switch(AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->conditions->condition_type)
				{
					case 1:
						comboBox5->Enabled= true;
						comboBox6->Enabled= true;
						//comboBox7->Enabled= true;
						//comboBox8->Enabled= true;
						comboBox3->SelectedIndex = AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->conditions->operator_id;
						comboBox7->SelectedIndex = AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->conditions->condition_left->condition_type-1;
						comboBox5->SelectedIndex = AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->conditions->condition_left->operator_id;
						comboBox8->SelectedIndex = AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->conditions->condition_right->condition_type-1;
						comboBox6->SelectedIndex = AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->conditions->condition_right->operator_id;
						if(AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->conditions->argument_bytes>0)
						{
							textBox9->Enabled=true;
							textBox9->Text=condition_value(AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->conditions);
						}
						else textBox9->Text=condition_value(AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->conditions);
						if(AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->conditions->condition_right->argument_bytes>0)
						{
							textBox10->Enabled=true;
							textBox10->Text=condition_value(AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->conditions->condition_right);
						}
						if(AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->conditions->condition_left->argument_bytes>0)
						{
							textBox11->Enabled=true;
							textBox11->Text=condition_value(AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->conditions->condition_left);
						}
					break;
					case 2:
						comboBox3->SelectedIndex = AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->conditions->operator_id;
						comboBox7->SelectedIndex = AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->conditions->condition_right->condition_type-1;
						comboBox5->SelectedIndex = AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->conditions->condition_right->operator_id;
						comboBox5->Enabled= true;
						comboBox7->Enabled= true;
						if(AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->conditions->argument_bytes>0)
						{
							textBox9->Enabled=true;
							textBox9->Text=condition_value(AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->conditions);
						}
						if(AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->conditions->condition_right->argument_bytes>0)
						{
							textBox10->Enabled=true;
							textBox10->Text=condition_value(AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->conditions->condition_right);
						}
					break;
					case 3:
						comboBox3->SelectedIndex = AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->conditions->operator_id;
						if(AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->conditions->argument_bytes>0)
						{
							textBox9->Enabled=true;
							textBox9->Text=condition_value(AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->conditions);
						}
					break;
				}
				
				
				
				if(soap && listBox_ActionSet->SelectedItem)
				{
					Cursor = Windows::Forms::Cursors::AppStarting;
					to = comboBox_language->SelectedItem->ToString();
					try
					{
						textBox_translation->Text = soap->Translate(appID, listBox_ActionSet->SelectedItem->ToString()->Split(gcnew array<wchar_t>{'\0'})[0], from, to, "text/plain", "general");
					}
					catch(...)
					{
						textBox_translation->Text = "TRANSLATION FAILED";
					}
					Cursor = Windows::Forms::Cursors::Default;
				}
			}
		}
		private: System::Void change_searchCat(System::Object^  sender, System::EventArgs^  e)
		{
			if(comboBox_cat->SelectedIndex > -1)
			{
				comboBox_subCat->Items->Clear();

				if(comboBox_cat->SelectedItem->ToString() == "Conditions")
				{
					comboBox_subCat->Items->Add("All");
					comboBox_subCat->SelectedIndex = 0;
				}

				if(comboBox_cat->SelectedItem->ToString() == "AI Control Link")
				{
					comboBox_subCat->Items->AddRange
					(
						gcnew array<String^>
						{
							"Task",
							"Element NPC"
						}
					);
					comboBox_subCat->SelectedIndex = 0;
				}

				if(comboBox_cat->SelectedItem->ToString() == "Procedures")
				{
					comboBox_subCat->Items->AddRange
					(
						gcnew array<String^>
						{
							"Attack",
							"Cast_Skill",
							"Broadcast_Message",
							"Reset_Aggro",
							"Execute_ActionSet",
							"Disable_ActionSet",
							"Enable_ActionSet",
							"Create_Timer",
							"Remove_Timer",
							"Run_Away",
							"Be_Taunted",
							"Fade_Target",
							"Fade_Aggro",
							"Break",
							"NPC_Generator",
							"Initialize_Public_Counter",
							"Increment_Public_Counter",
							"Player_Aimed_NPC_Spawn",
							"Change_Path",
							"Play_Action",
							"Unknown1",
							"Unknown2",
							"Unknown3",
							"Unknown4",
							"Unknown5",
							"Unknown6",
							"Unknown7",
							"Unknown8",
							"Unknown9",
							"Unknown10",
							"Unknown11",
							"Unknown12"
						}
					);
					comboBox_subCat->SelectedIndex = 0;
				}

				if(comboBox_cat->SelectedItem->ToString() == "Targets")
				{
					comboBox_subCat->Items->AddRange
					(
						gcnew array<String^>
						{
							"AGGRO_FIRST",
							"AGGRO_SECOND",
							"AGGRO_SECOND_RAND",
							"MOST_HP",
							"MOST_MP",
							"LEAST_HP",
							"CLASS_COMBO",
							"SELF"
						}
					);
					comboBox_subCat->SelectedIndex = 0;
				}
			}
		}
		private: System::Void click_search(System::Object^  sender, System::EventArgs^  e)
		{
			if(AI && comboBox_cat->SelectedIndex > -1)
			{
				// get initial search start
				int ac = 0;
				int as = 0;
				int pc = 0;

				if(listBox_ActionController->SelectedIndex > -1)
				{
					ac = listBox_ActionController->SelectedIndex;
				}
				if(listBox_ActionSet->SelectedIndex > -1)
				{
					as = listBox_ActionSet->SelectedIndex;
					if(comboBox_cat->SelectedItem->ToString() == "Conditions")
					{
						as++;
					}					
				}
				if(listBox_Procedures->SelectedIndex > -1)
				{
					pc = listBox_Procedures->SelectedIndex;
					if(comboBox_cat->SelectedItem->ToString() == "Procedures")
					{
						pc++;
					}	
				}

				for(ac; ac<AI->action_controllers->Length; ac++)
				{	
					if(comboBox_cat->SelectedItem->ToString() == "AI Control Link")
					{
						if(comboBox_subCat->SelectedIndex > -1 && textBox_pattern->Text != "")
						{
							int id = 0;
							if(comboBox_subCat->SelectedItem->ToString() == "Task")
							{
								MessageBox::Show("Task AI Control Link decryption currently not discovered...");
								return;
								id = Convert::ToInt32(textBox_pattern->Text)-55;
							}
							if(comboBox_subCat->SelectedItem->ToString() == "Element NPC")
							{
								id = Convert::ToInt32(textBox_pattern->Text);
							}
							if(AI->action_controllers[ac]->id == id)
							{
								listBox_ActionController->SelectedIndex = ac;
								return;
							}
						}
					}
					for(as; as<AI->action_controllers[ac]->action_sets->Length; as++)
					{
						if(comboBox_cat->SelectedItem->ToString() == "Conditions")
						{
							if(textBox_pattern->Text != "" && condition_expression(AI->action_controllers[ac]->action_sets[as]->conditions)->Contains(textBox_pattern->Text))
							{
								listBox_ActionController->SelectedIndex = ac;
								listBox_ActionSet->SelectedIndex = as;
								return;
							}
						}

						if(comboBox_cat->SelectedItem->ToString() == "Procedures" && comboBox_subCat->SelectedIndex > -1)
						{
							for(pc; pc<AI->action_controllers[ac]->action_sets[as]->procedures->Length; pc++)
							{
								if(AI->action_controllers[ac]->action_sets[as]->procedures[pc]->type == comboBox_subCat->SelectedIndex)
								{
									if(textBox_pattern->Text == "" || procedure_expression(AI->action_controllers[ac]->action_sets[as]->procedures[pc])->Contains(textBox_pattern->Text))
									{
										listBox_ActionController->SelectedIndex = ac;
										listBox_ActionSet->SelectedIndex = as;
										listBox_Procedures->SelectedIndex = pc;
										return;
									}
								}
							}
							pc = 0;
						}

						if(comboBox_cat->SelectedItem->ToString() == "Targets" && comboBox_subCat->SelectedIndex > -1)
						{
							for(pc; pc<AI->action_controllers[ac]->action_sets[as]->procedures->Length; pc++)
							{
								if(AI->action_controllers[ac]->action_sets[as]->procedures[pc]->target_type == comboBox_subCat->SelectedIndex)
								{
									if(textBox_pattern->Text == "" || procedure_expression(AI->action_controllers[ac]->action_sets[as]->procedures[pc])->Contains(textBox_pattern->Text))
									{
										listBox_ActionController->SelectedIndex = ac;
										listBox_ActionSet->SelectedIndex = as;
										listBox_Procedures->SelectedIndex = pc;
										return;
									}
								}
							}
							pc = 0;
						}
					}
					as = 0;
				}
			}
			MessageBox::Show("Search reached End of File without Result!");
		}
		private: System::Void click_importText(System::Object^  sender, System::EventArgs^  e)
		{
			OpenFileDialog^ load = gcnew OpenFileDialog();
			load->InitialDirectory = Environment::CurrentDirectory;
			load->Filter = "Text File (*.txt)|*.txt|All Files (*.*)|*.*";
			if(load->ShowDialog() == Windows::Forms::DialogResult::OK && File::Exists(load->FileName))
			{
				try
				{
					StreamReader^ sr = gcnew StreamReader(load->FileName);

					String^ line;
					int counter;
					array<String^>^ pair;
					array<String^>^ pair2;
					array<String^>^ seperator = gcnew array<String^>{"\t"};
					array<String^>^ seperator2 = gcnew array<String^>{"-"};
					int test=0;
					while(!sr->EndOfStream)
					{
						line = sr->ReadLine();
						if(!line->StartsWith("#") && line->Contains("\t"))
						{
							pair = line->Split(seperator, StringSplitOptions::RemoveEmptyEntries);
							if(pair->Length == 2)
							{
								pair2 = pair[0]->Split(seperator2, StringSplitOptions::RemoveEmptyEntries);
								;
								int pr_type=AI->action_controllers[Convert::ToInt32(pair2[0])]->action_sets[Convert::ToInt32(pair2[1])]->procedures[Convert::ToInt32(pair2[2])]->type;
								switch(pr_type){
									case 2:
										counter=pair[1]->Length*2+2;
										AI->action_controllers[Convert::ToInt32(pair2[0])]->action_sets[Convert::ToInt32(pair2[1])]->procedures[Convert::ToInt32(pair2[2])]->parameters[0] = counter;
										AI->action_controllers[Convert::ToInt32(pair2[0])]->action_sets[Convert::ToInt32(pair2[1])]->procedures[Convert::ToInt32(pair2[2])]->parameters[1]=string_to_bytes(pair[1]->Replace("\r\n","")+"\0", "Unicode", counter);
									break;
									case 15:
										AI->action_controllers[Convert::ToInt32(pair2[0])]->action_sets[Convert::ToInt32(pair2[1])]->procedures[Convert::ToInt32(pair2[2])]->parameters[4]=string_to_bytes(pair[1]->Replace("\r\n","")+"\0", "Unicode", 32);
									break;
									case 19:
										AI->action_controllers[Convert::ToInt32(pair2[0])]->action_sets[Convert::ToInt32(pair2[1])]->procedures[Convert::ToInt32(pair2[2])]->parameters[0]=string_to_bytes(pair[1]->Replace("\r\n","")+"\0", "Unicode", 128);
									break;
									default:

									break;
								}
								//for(int i=0; i<eLC->Lists[table]->elementValues->Length; i++)
								//{
									test++;
									//if(eLC->GetValue(table, i, id_pos)==pair[0]) eLC->SetValue(table, i, name_pos, pair[1]->Replace("\\r","\r\n"));
								//}
								//MessageBox::Show("TEST: " + pair[0] +":"+pair[1]);
								//eLC->SetValue(table, n, f, (String^)skillTable[skill]);
								//skillTable->Add(pair[0], pair[1]);
							}
						}
					}
					MessageBox::Show("IMPORT TEXTS "+test);
					/*
					for(int ac=0; ac<AI->action_controllers->Length; ac++)
					{
						for(int as=0; as<AI->action_controllers[ac]->action_sets->Length; as++)
						{
							for(int p=0; p<AI->action_controllers[ac]->action_sets[as]->procedures->Length; p++)
							{
								int pr_type=AI->action_controllers[ac]->action_sets[as]->procedures[p]->type;
								switch(pr_type){
								case 2:
									sw->WriteLine(ac+"-"+as+"-"+p+"\t"+(Encoding::Unicode)->GetString((array<unsigned char>^)AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[1])->Replace("\0", ""));
								break;
								case 15:
									sw->WriteLine(ac+"-"+as+"-"+p+"\t"+(Encoding::Unicode)->GetString((array<unsigned char>^)AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[4])->Replace("\0", ""));
								break;
								case 19:
									sw->WriteLine(ac+"-"+as+"-"+p+"\t"+(Encoding::Unicode)->GetString((array<unsigned char>^)AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[0])->Replace("\0", ""));
								break;
								default:

								break;
								}
							}
						}
					}
					*/
					sr->Close();
				}
				catch(...)
				{
					MessageBox::Show("SAVING ERROR!");
				}
			}
		}
		private: System::Void click_exportText(System::Object^  sender, System::EventArgs^  e)
		{
			SaveFileDialog^ save = gcnew SaveFileDialog();
			save->InitialDirectory = Environment::CurrentDirectory;
			save->Filter = "Text File (*.txt)|*.txt|All Files (*.*)|*.*";
			if(save->ShowDialog() == Windows::Forms::DialogResult::OK && save->FileName != "")
			{
				try
				{
					//Cursor = Windows::Forms::Cursors::AppStarting;
					StreamWriter^ sw = gcnew StreamWriter(save->FileName, false, Encoding::Unicode);
					MessageBox::Show("EXPORT TEXTS ");
					System::Text::Encoding^ enc = System::Text::Encoding::GetEncoding("Unicode");
					//return enc->GetString(value);

					for(int ac=0; ac<AI->action_controllers->Length; ac++)
					{
						for(int as=0; as<AI->action_controllers[ac]->action_sets->Length; as++)
						{
							for(int p=0; p<AI->action_controllers[ac]->action_sets[as]->procedures->Length; p++)
							{
								int pr_type=AI->action_controllers[ac]->action_sets[as]->procedures[p]->type;
								switch(pr_type){
								case 2:
									sw->WriteLine(ac+"-"+as+"-"+p+"\t"+(Encoding::Unicode)->GetString((array<unsigned char>^)AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[1])->Replace("\0", ""));
								break;
								case 15:
									sw->WriteLine(ac+"-"+as+"-"+p+"\t"+(Encoding::Unicode)->GetString((array<unsigned char>^)AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[4])->Replace("\0", ""));
								break;
								case 19:
									sw->WriteLine(ac+"-"+as+"-"+p+"\t"+(Encoding::Unicode)->GetString((array<unsigned char>^)AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[0])->Replace("\0", ""));
								break;
								default:

								break;
								}
							}
						}
					}

					sw->Close();
				}
				catch(...)
				{
					MessageBox::Show("SAVING ERROR!");
				}
			}
		}
		private: System::Void click_analyze(System::Object^  sender, System::EventArgs^  e)
		{
			int condition_type = 0;
			int procedure_type = 0;
			int procedure_target_type = 0;

			for(int ac=0; ac<AI->action_controllers->Length; ac++)
			{
				for(int as=0; as<AI->action_controllers[ac]->action_sets->Length; as++)
				{
					condition_type = Math::Max(condition_type, AI->action_controllers[ac]->action_sets[as]->conditions->operator_id);
					for(int p=0; p<AI->action_controllers[ac]->action_sets[as]->procedures->Length; p++)
					{
						procedure_type = Math::Max(procedure_type, AI->action_controllers[ac]->action_sets[as]->procedures[p]->type);
						procedure_target_type = Math::Max(procedure_target_type, AI->action_controllers[ac]->action_sets[as]->procedures[p]->target_type);
					}
				}
			}

			MessageBox::Show("Condition [18]: " + condition_type + "\nProcedure [19]: " + procedure_type + "\nTarget Type [7]: " + procedure_target_type, "Analyzer");
		}
		private: System::Void click_deleteID(System::Object^  sender, System::EventArgs^  e)
		{
			if(contextMenuStrip1->SourceControl == listBox_ActionController)
			{
				if(AI && listBox_ActionController->SelectedIndex>-1)
				{
					//MessageBox::Show("deleted");
					AI->action_controllers_count--;
					array <ActionController^>^ temp = gcnew array<ActionController^>(AI->action_controllers_count);
					Array::Copy(AI->action_controllers, 0, temp, 0, listBox_ActionController->SelectedIndex);
					Array::Copy(AI->action_controllers, listBox_ActionController->SelectedIndex+1, temp, listBox_ActionController->SelectedIndex, AI->action_controllers_count - listBox_ActionController->SelectedIndex);
					AI->action_controllers = temp;

					listBox_ActionController->Items->Clear();
					for(int i=0; i<AI->action_controllers->Length; i++)
						{
							listBox_ActionController->Items->Add(AI->action_controllers[i]->id.ToString());
						}
				}
			}
			if(contextMenuStrip1->SourceControl == listBox_ActionSet)
			{
				if(AI && listBox_ActionSet->SelectedIndex>-1)
				{
					//MessageBox::Show("deleted");
					int index=listBox_ActionController->SelectedIndex;
					int index2=listBox_ActionSet->SelectedIndex;
					AI->action_controllers[index]->action_sets_count--;
					array <ActionSet^>^ temp = gcnew array<ActionSet^>(AI->action_controllers[index]->action_sets_count);
					Array::Copy(AI->action_controllers[index]->action_sets, 0, temp, 0, index2);
					Array::Copy(AI->action_controllers[index]->action_sets, index2+1, temp, index2, AI->action_controllers[index]->action_sets_count - index2);
					AI->action_controllers[index]->action_sets = temp;

					listBox_ActionSet->Items->Clear();
					for(int i=0; i<AI->action_controllers[index]->action_sets->Length; i++)
						{
							listBox_ActionSet->Items->Add("[" + AI->action_controllers[index]->action_sets[i]->id.ToString() + "] " + AI->action_controllers[index]->action_sets[i]->Name);
						}
				}
			}
			if(contextMenuStrip1->SourceControl == listBox_Procedures)
			{
				if(AI && listBox_Procedures->SelectedIndex>-1)
				{
					//MessageBox::Show("deleted");
					int index=listBox_ActionController->SelectedIndex;
					int index2=listBox_ActionSet->SelectedIndex;
					int index3=listBox_Procedures->SelectedIndex;
					AI->action_controllers[index]->action_sets[index2]->procedures_count--;
					array <Procedure^>^ temp = gcnew array<Procedure^>(AI->action_controllers[index]->action_sets[index2]->procedures_count);
					Array::Copy(AI->action_controllers[index]->action_sets[index2]->procedures, 0, temp, 0, index3);
					Array::Copy(AI->action_controllers[index]->action_sets[index2]->procedures, index3+1, temp, index3, AI->action_controllers[index]->action_sets[index2]->procedures_count - index3);
					AI->action_controllers[index]->action_sets[index2]->procedures = temp;

					listBox_Procedures->Items->Clear();
					for(int i=0; i<AI->action_controllers[index]->action_sets[index2]->procedures->Length; i++)
						{
							listBox_Procedures->Items->Add("[" + i.ToString() + "] " + procedure_expression(AI->action_controllers[index]->action_sets[index2]->procedures[i]));
						}
				}
			}
			
		}
		private: System::Void click_addID(System::Object^  sender, System::EventArgs^  e)
		{
			if(contextMenuStrip1->SourceControl == listBox_ActionController)
			{
				if(AI) //&& listBox_Cats->SelectedIndex>-1
				{
					int last_index=AI->action_controllers_count;
					AI->action_controllers_count++;
					Array::Resize(AI->action_controllers, AI->action_controllers_count);
					AI->action_controllers[last_index] = gcnew ActionController();
					AI->action_controllers[last_index]->signature = 0; 
					AI->action_controllers[last_index]->id = AI->action_controllers[last_index-1]->id+1;
					AI->action_controllers[last_index]->action_sets_count = 0;
					AI->action_controllers[last_index]->action_sets = gcnew array<ActionSet^>(0);
					listBox_ActionController->Items->Add(AI->action_controllers[last_index]->id.ToString());
					listBox_ActionController->SelectedIndex = listBox_ActionController->Items->Count-1;
				}
			}
			if(contextMenuStrip1->SourceControl == listBox_ActionSet)
			{
				if(AI) //&& listBox_Cats->SelectedIndex>-1
				{
					int index=listBox_ActionController->SelectedIndex;
					int last_index=AI->action_controllers[index]->action_sets_count;
					AI->action_controllers[index]->action_sets_count++;
					Array::Resize(AI->action_controllers[index]->action_sets, AI->action_controllers[index]->action_sets_count);
					AI->action_controllers[index]->action_sets[last_index] = gcnew ActionSet();
					AI->action_controllers[index]->action_sets[last_index]->version = 11;
					AI->action_controllers[index]->action_sets[last_index]->id = last_index;
					AI->action_controllers[index]->action_sets[last_index]->flags = AI->action_controllers[0]->action_sets[0]->flags;
					AI->action_controllers[index]->action_sets[last_index]->name = string_to_bytes("New", "Unicode", 128);
					AI->action_controllers[index]->action_sets[last_index]->conditions = gcnew Condition();
					AI->action_controllers[index]->action_sets[last_index]->conditions->operator_id=0;
					AI->action_controllers[index]->action_sets[last_index]->conditions->argument_bytes=4;
					AI->action_controllers[index]->action_sets[last_index]->conditions->value=string_to_bytes("", "Unicode", 4);
					AI->action_controllers[index]->action_sets[last_index]->conditions->condition_type=3;
					//MessageBox::Show("deleted"+AI->action_controllers[index]->action_sets[last_index]->name->ToString());
					AI->action_controllers[index]->action_sets[last_index]->procedures_count = 0;
					AI->action_controllers[index]->action_sets[last_index]->procedures = gcnew array<Procedure^>(AI->action_controllers[index]->action_sets[last_index]->procedures_count);
					listBox_ActionSet->Items->Add("[" + AI->action_controllers[index]->action_sets[last_index]->id.ToString() + "] " + bytes_to_string(AI->action_controllers[index]->action_sets[last_index]->name, "Unicode"));
					listBox_ActionSet->SelectedIndex = listBox_ActionSet->Items->Count-1;
				
				}
			}
			if(contextMenuStrip1->SourceControl == listBox_Procedures)
			{
				if(AI) //&& listBox_Cats->SelectedIndex>-1
				{
					int index=listBox_ActionController->SelectedIndex;
					int index2=listBox_ActionSet->SelectedIndex;
					//int index3=listBox_Procedures->SelectedIndex;
					int last_index=AI->action_controllers[index]->action_sets[index2]->procedures_count;
					AI->action_controllers[index]->action_sets[index2]->procedures_count++;

					Array::Resize(AI->action_controllers[index]->action_sets[index2]->procedures, AI->action_controllers[index]->action_sets[index2]->procedures_count);
					AI->action_controllers[index]->action_sets[index2]->procedures[last_index] = gcnew Procedure();
					AI->action_controllers[index]->action_sets[index2]->procedures[last_index]->type = 2;
					AI->action_controllers[index]->action_sets[index2]->procedures[last_index]->parameters = gcnew array<Object^>{6, Encoding::Unicode->GetBytes("...")};
					AI->action_controllers[index]->action_sets[index2]->procedures[last_index]->target_type = 0;
					AI->action_controllers[index]->action_sets[index2]->procedures[last_index]->target_parameters = gcnew array<Object^>{};
				
					listBox_Procedures->Items->Add("[" + last_index.ToString() + "] " + procedure_expression(AI->action_controllers[index]->action_sets[index2]->procedures[last_index]));
					listBox_Procedures->SelectedIndex = listBox_Procedures->Items->Count-1;
				}
			}
		}
		private: System::Void click_dupID(System::Object^  sender, System::EventArgs^  e)
		{
			if(contextMenuStrip1->SourceControl == listBox_ActionController)
			{
				if(AI && listBox_ActionController->SelectedIndex>-1)
				{
					int index=listBox_ActionController->SelectedIndex;
					int last_index=AI->action_controllers_count;
					AI->action_controllers_count++;
					Array::Resize(AI->action_controllers, AI->action_controllers_count);
					AI->action_controllers[last_index] = gcnew ActionController();
					AI->action_controllers[last_index]->signature = AI->action_controllers[index]->signature; 
					AI->action_controllers[last_index]->id = AI->action_controllers[last_index-1]->id+1;
					AI->action_controllers[last_index]->action_sets_count = AI->action_controllers[index]->action_sets_count;
					AI->action_controllers[last_index]->action_sets = gcnew array<ActionSet^>(AI->action_controllers[last_index]->action_sets_count);
					for(int as=0; as<AI->action_controllers[index]->action_sets->Length; as++)
						{
							AI->action_controllers[last_index]->action_sets[as] = gcnew ActionSet();
							AI->action_controllers[last_index]->action_sets[as]->version = AI->action_controllers[index]->action_sets[as]->version;
							AI->action_controllers[last_index]->action_sets[as]->id = AI->action_controllers[index]->action_sets[as]->id;
							AI->action_controllers[last_index]->action_sets[as]->flags = AI->action_controllers[index]->action_sets[as]->flags;
							AI->action_controllers[last_index]->action_sets[as]->name = AI->action_controllers[index]->action_sets[as]->name;
							AI->action_controllers[last_index]->action_sets[as]->conditions = gcnew Condition();
							AI->action_controllers[last_index]->action_sets[as]->conditions = AI->action_controllers[index]->action_sets[as]->conditions;
							AI->action_controllers[last_index]->action_sets[as]->procedures_count = AI->action_controllers[index]->action_sets[as]->procedures_count;
							AI->action_controllers[last_index]->action_sets[as]->procedures = gcnew array<Procedure^>(AI->action_controllers[last_index]->action_sets[as]->procedures_count);
							for(int p=0; p<AI->action_controllers[index]->action_sets[as]->procedures->Length; p++)
							{
								AI->action_controllers[last_index]->action_sets[as]->procedures[p] = gcnew Procedure();
								AI->action_controllers[last_index]->action_sets[as]->procedures[p]->type = AI->action_controllers[index]->action_sets[as]->procedures[p]->type;
								AI->action_controllers[last_index]->action_sets[as]->procedures[p]->parameters = AI->action_controllers[index]->action_sets[as]->procedures[p]->parameters;
								AI->action_controllers[last_index]->action_sets[as]->procedures[p]->target_type = AI->action_controllers[index]->action_sets[as]->procedures[p]->target_type;
							}
						}
					listBox_ActionController->Items->Add(AI->action_controllers[last_index]->id.ToString());
					listBox_ActionController->SelectedIndex = listBox_ActionController->Items->Count-1;
				}
			}
			if(contextMenuStrip1->SourceControl == listBox_ActionSet)
			{
				if(AI&& listBox_ActionSet->SelectedIndex>-1)
				{
					int index=listBox_ActionController->SelectedIndex;
					int index2=listBox_ActionSet->SelectedIndex;
					int last_index=AI->action_controllers[index]->action_sets_count;
					AI->action_controllers[index]->action_sets_count++;
					Array::Resize(AI->action_controllers[index]->action_sets, AI->action_controllers[index]->action_sets_count);
					AI->action_controllers[index]->action_sets[last_index] = gcnew ActionSet();
					AI->action_controllers[index]->action_sets[last_index]->version = AI->action_controllers[index]->action_sets[index2]->version;
					AI->action_controllers[index]->action_sets[last_index]->id = last_index;
					AI->action_controllers[index]->action_sets[last_index]->flags =AI->action_controllers[index]->action_sets[index2]->flags;
					AI->action_controllers[index]->action_sets[last_index]->name = AI->action_controllers[index]->action_sets[index2]->name;
					AI->action_controllers[index]->action_sets[last_index]->conditions = gcnew Condition();
					AI->action_controllers[index]->action_sets[last_index]->conditions = AI->action_controllers[index]->action_sets[index2]->conditions;
					
					AI->action_controllers[index]->action_sets[last_index]->procedures_count = AI->action_controllers[index]->action_sets[index2]->procedures_count;
					AI->action_controllers[index]->action_sets[last_index]->procedures = gcnew array<Procedure^>(AI->action_controllers[index]->action_sets[last_index]->procedures_count);
					for(int p=0; p<AI->action_controllers[index]->action_sets[index2]->procedures->Length; p++)
						{
							AI->action_controllers[index]->action_sets[last_index]->procedures[p] = gcnew Procedure();
							AI->action_controllers[index]->action_sets[last_index]->procedures[p]->type = AI->action_controllers[index]->action_sets[index2]->procedures[p]->type;
							AI->action_controllers[index]->action_sets[last_index]->procedures[p]->parameters = AI->action_controllers[index]->action_sets[index2]->procedures[p]->parameters;
							AI->action_controllers[index]->action_sets[last_index]->procedures[p]->target_type = AI->action_controllers[index]->action_sets[index2]->procedures[p]->target_type;
						}
					listBox_ActionSet->Items->Add("[" + AI->action_controllers[index]->action_sets[last_index]->id.ToString() + "] " + AI->action_controllers[index]->action_sets[last_index]->Name);//bytes_to_string(AI->action_controllers[index]->action_sets[last_index]->name, "Unicode"));
					listBox_ActionSet->SelectedIndex = listBox_ActionSet->Items->Count-1;
				
				}
			}
			if(contextMenuStrip1->SourceControl == listBox_Procedures)
			{
				if(AI && listBox_Procedures->SelectedIndex>-1) //
				{
					int index=listBox_ActionController->SelectedIndex;
					int index2=listBox_ActionSet->SelectedIndex;
					int index3=listBox_Procedures->SelectedIndex;
					int last_index=AI->action_controllers[index]->action_sets[index2]->procedures_count;
					AI->action_controllers[index]->action_sets[index2]->procedures_count++;
					Array::Resize(AI->action_controllers[index]->action_sets[index2]->procedures, AI->action_controllers[index]->action_sets[index2]->procedures_count);
					AI->action_controllers[index]->action_sets[index2]->procedures[last_index] = gcnew Procedure();
					AI->action_controllers[index]->action_sets[index2]->procedures[last_index]->type = AI->action_controllers[index]->action_sets[index2]->procedures[index3]->type;
					AI->action_controllers[index]->action_sets[index2]->procedures[last_index]->parameters = AI->action_controllers[index]->action_sets[index2]->procedures[index3]->parameters;
					AI->action_controllers[index]->action_sets[index2]->procedures[last_index]->target_type = AI->action_controllers[index]->action_sets[index2]->procedures[index3]->target_type;
				
					listBox_Procedures->Items->Add("[" + last_index.ToString() + "] " + procedure_expression(AI->action_controllers[index]->action_sets[index2]->procedures[last_index]));
					listBox_Procedures->SelectedIndex = listBox_Procedures->Items->Count-1;
				}
			}
		}
		private: System::Void change_arguments(System::Object^  sender, System::EventArgs^  e)
		{
			if(AI && listBox_Procedures->SelectedIndex > -1) //&& textBox2->Enabled == true
			{
				int ac = listBox_ActionController->SelectedIndex;
				int as = listBox_ActionSet->SelectedIndex;
				int p = listBox_Procedures->SelectedIndex;
				
				if(textBox2->Enabled==true&&AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[0] != Convert::ToInt32(textBox2->Text)){
					AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[0] = Convert::ToInt32(textBox2->Text);
				}
				
				int count;
				switch(AI->action_controllers[ac]->action_sets[as]->procedures[p]->type){
				case 2:
					count=textBox3->TextLength*2+2;
					//MessageBox::Show("?"+count+"?"+textBox3->Text);
					AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[0] = Convert::ToInt32(count);
					AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[1] = string_to_bytes(textBox3->Text+"\0", "Unicode", count);
					break;
				case 14:
					if(textBox3->Enabled==true&&AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[1] != Convert::ToInt16(textBox3->Text)){
						AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[1] = Convert::ToInt16(textBox3->Text);
					}
					break;
				case 19:
					AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[1] = string_to_bytes(textBox3->Text+"\0", "Unicode", 128);
					break;
				default:
					if(textBox3->Enabled==true&&AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[1] != Convert::ToInt32(textBox3->Text)){
						AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[1] = Convert::ToInt32(textBox3->Text);
					}
					break;
				}
				switch(AI->action_controllers[ac]->action_sets[as]->procedures[p]->type){
				case 14:
					if(textBox4->Enabled==true&&AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[2] != Convert::ToInt16(textBox4->Text)){
						AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[2] = Convert::ToInt16(textBox4->Text);
					}
					break;
				default:
					if(textBox4->Enabled==true&&AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[2] != Convert::ToInt32(textBox4->Text)){
						AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[2] = Convert::ToInt32(textBox4->Text);
					}
					break;
				}
				switch(AI->action_controllers[ac]->action_sets[as]->procedures[p]->type){
				case 25:
					if(textBox5->Enabled==true&&AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[3] != Convert::ToInt16(textBox5->Text)){
						AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[3] = Convert::ToInt16(textBox5->Text);
					}
					break;
				default:
					if(textBox5->Enabled==true&&AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[3] != Convert::ToInt32(textBox5->Text)){
						AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[3] = Convert::ToInt32(textBox5->Text);
					}
					break;
				}
				switch(AI->action_controllers[ac]->action_sets[as]->procedures[p]->type){
				case 15:
					AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[4] = string_to_bytes(textBox6->Text+"\0", "Unicode", 32);
					break;
				case 25:
					if(textBox6->Enabled==true&&AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[4] != Convert::ToInt16(textBox6->Text)){
						AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[4] = Convert::ToInt16(textBox6->Text);
					}
					break;
				default:
					if(textBox6->Enabled==true&&AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[4] != Convert::ToInt32(textBox6->Text)){
						AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[4] = Convert::ToInt32(textBox6->Text);
					}
				break;
				}
				if(textBox7->Enabled==true&&AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[5] != Convert::ToInt32(textBox7->Text)){
					AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[5] = Convert::ToInt32(textBox7->Text);
				}
				if(textBox8->Enabled==true&&AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[6] != Convert::ToInt32(textBox8->Text)){
					AI->action_controllers[ac]->action_sets[as]->procedures[p]->parameters[6] = Convert::ToInt32(textBox8->Text);
				}
				listBox_Procedures->Items->Clear();
				for(int i=0; i<AI->action_controllers[ac]->action_sets[as]->procedures->Length; i++)
				{
					listBox_Procedures->Items->Add("[" + i.ToString() + "] " + procedure_expression(AI->action_controllers[ac]->action_sets[as]->procedures[i]));
				}
			}
		}
		private: System::Void change_cvalues(System::Object^  sender, System::EventArgs^  e)
		{
			if(AI && listBox_ActionSet->SelectedIndex > -1) //&& textBox2->Enabled == true
			{
				int ac = listBox_ActionController->SelectedIndex;
				int as = listBox_ActionSet->SelectedIndex;
				//int p = listBox_Procedures->SelectedIndex;
				//MessageBox::Show("test"+as);
				if(textBox9->Enabled==true&&AI->action_controllers[ac]->action_sets[as]->conditions->argument_bytes>0){
					AI->action_controllers[ac]->action_sets[as]->conditions->value=condition_value_convert(AI->action_controllers[ac]->action_sets[as]->conditions->operator_id, textBox9->Text);
				}
				if(textBox10->Enabled==true&&AI->action_controllers[ac]->action_sets[as]->conditions->condition_right->argument_bytes>0){
					AI->action_controllers[ac]->action_sets[as]->conditions->condition_right->value=condition_value_convert(AI->action_controllers[ac]->action_sets[as]->conditions->condition_right->operator_id, textBox10->Text);
				}
				if(textBox11->Enabled==true&&AI->action_controllers[ac]->action_sets[as]->conditions->condition_left->argument_bytes>0){
					AI->action_controllers[ac]->action_sets[as]->conditions->condition_left->value=condition_value_convert(AI->action_controllers[ac]->action_sets[as]->conditions->condition_left->operator_id, textBox11->Text);
				}
				
				textBox_Condition->Text = "IF{ " + condition_expression(AI->action_controllers[ac]->action_sets[as]->conditions) + " }";
			}
		}
		int condition_arg_bytes(int operator_id){
			if(operator_id == 0)
			{
				return 4;
			}
			if(operator_id == 1)
			{
				return 4;
			}
			if(operator_id == 3)
			{
				return 4;
			}
			if(operator_id == 9)
			{
				return 4;
			}
			if(operator_id == 10)
			{
				return 8;
			}
			if(operator_id == 11)
			{
				return 4;
			}
			if(operator_id == 19)
			{
				return 4;
			}
			if(operator_id == 20)
			{
				return 4;
			}
			return 0;
		}
		private: System::Void change_condition_option(System::Object^  sender, System::EventArgs^  e)
		{
			if(AI && listBox_ActionSet->SelectedIndex > -1)
			{
				int ac = listBox_ActionController->SelectedIndex;
				int as = listBox_ActionSet->SelectedIndex;
				//int p = listBox_Procedures->SelectedIndex;
				//MessageBox::Show("test"+as);
				if( AI->action_controllers[ac]->action_sets[as]->conditions->operator_id != comboBox3->SelectedIndex ){
					AI->action_controllers[ac]->action_sets[as]->conditions->operator_id=comboBox3->SelectedIndex;
					AI->action_controllers[ac]->action_sets[as]->conditions->argument_bytes=condition_arg_bytes(comboBox3->SelectedIndex);
					AI->action_controllers[ac]->action_sets[as]->conditions->value=condition_value_convert(comboBox3->SelectedIndex, "0");
					textBox_Condition->Text = "IF{ " + condition_expression(AI->action_controllers[ac]->action_sets[as]->conditions) + " }";
				}
			}
		}
		private: System::Void rename_ac(System::Object^  sender, System::EventArgs^  e)
		{
			if(AI && listBox_ActionSet->SelectedIndex>-1)
			{
				AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->name=string_to_bytes(textBox1->Text->ToString(), "GBK", 128);
				listBox_ActionSet->Items[listBox_ActionSet->SelectedIndex] = "[" + AI->action_controllers[listBox_ActionController->SelectedIndex]->action_sets[listBox_ActionSet->SelectedIndex]->id.ToString() + "] " + textBox1->Text;
			}
		}
		private: System::Void click_exportDebug(System::Object^  sender, System::EventArgs^  e)
		{
			SaveFileDialog^ save = gcnew SaveFileDialog();
			save->Filter = "AI Policy (*.data)|*.data|All Files (*.*)|*.*";
			if(save->ShowDialog() == Windows::Forms::DialogResult::OK && save->FileName != "")
			{
				Cursor = Windows::Forms::Cursors::WaitCursor;

				FileStream^ fw = gcnew FileStream(save->FileName, FileMode::Create, FileAccess::Write);
				BinaryWriter^ bw = gcnew BinaryWriter(fw);

				int action_controller_count = 2000;

				bw->Write(0); // 0
				bw->Write(action_controller_count);
				for(int ac=0; ac<action_controller_count; ac++)
				{
					bw->Write(0); // 0
					bw->Write(ac+1); // id
					bw->Write(1); // actionset count

					// write action set
					bw->Write(1); // version = 1
					bw->Write(0); // action set id
					bw->Write(gcnew array<unsigned char>{1, 0, 0}); // flags
					bw->Write(gcnew array<unsigned char>(128)); // name
					// write condition
					bw->Write(5); // operator id
					bw->Write(0); // argument bytes
					bw->Write(2); // condition type
						// rite right hand condition
						bw->Write(1); // operator id
						bw->Write(4); // argument bytes
						bw->Write((float)0.1); // argument value
						bw->Write(3); // condition type
						// return to parent condition
					bw->Write(4); // subnode2
					
					// write procedure
					bw->Write(1); // procedure count

					bw->Write(2); // broadcast
					bw->Write(46); // char count
					String^ message = "AI CONTROLLER ID: " + (ac+1).ToString("D5");
					bw->Write(Encoding::Unicode->GetBytes(message)); // unicode chars
					bw->Write(0); // target
					// no extra
				}

				bw->Close();
				fw->Close();;

				Cursor = Windows::Forms::Cursors::Default;
			}
		}
};