#pragma once

#include "task.h"

void WriteDate(int version, BinaryWriter^ bw, Date^ date)
{
	bw->Write(date->year);
	bw->Write(date->month);
	bw->Write(date->day);
	bw->Write(date->hour);
	bw->Write(date->minute);
	bw->Write(date->weekday);
}

void WriteSW(int version, BinaryWriter^ bw, ShengWang^ wang)
{
	bw->Write(wang->GuiDao);
	bw->Write(wang->QingYun);
	bw->Write(wang->TianYin);
	bw->Write(wang->GuiWang);
	bw->Write(wang->HeHuan);
	bw->Write(wang->ZhongYi);
	bw->Write(wang->QingYuan);
	bw->Write(wang->WenCai);
	bw->Write(wang->ShiDe);
	bw->Write(wang->DaoXin);
	bw->Write(wang->MoXing);
	bw->Write(wang->FoYuan);
	bw->Write(wang->unknow_15);
	bw->Write(wang->JiuLi);
	bw->Write(wang->LieShan);
	bw->Write(wang->ChenHuang);
	bw->Write(wang->TaiHao);
	bw->Write(wang->HuaiGuang);
	bw->Write(wang->TianHua);
	bw->Write(wang->FenXiang);
	bw->Write(wang->ZhanJi);
	bw->Write(wang->ZhanXun);
	bw->Write(wang->TianZong);
	bw->Write(wang->DiZong);
	bw->Write(wang->RenZong);
}
void WriteExpValItems(int version, BinaryWriter^ bw, EXP_Formalu^ formalu)
{
	bw->Write(formalu->val_1);
	bw->Write(formalu->val_2);
}

void WriteValueSet(int version, BinaryWriter^ bw, ValueSet^ value_set)
{
	bw->Write(value_set->val_1);
	bw->Write(value_set->val_2);
	bw->Write(value_set->val_3);
	bw->Write(value_set->val_count);
}

void WriteValueItem(int version, BinaryWriter^ bw, ValueItem^ value_item)
{
	bw->Write(value_item->val_1);
	bw->Write(value_item->val_2);
	bw->Write(value_item->val_3);
}

void WriteDateSpan(int version, BinaryWriter^ bw, DateSpan^ date_span)
{
	WriteDate(version, bw, date_span->from);
	WriteDate(version, bw, date_span->to);
}

void WriteLocation(int version, BinaryWriter^ bw, Map_Location^ location)
{
	bw->Write(location->map_id);
	bw->Write(location->x);
	bw->Write(location->altitude);
	bw->Write(location->z);
}

void WriteItem(int version, BinaryWriter^ bw, Item^ item)
{
	bw->Write(item->id);
	bw->Write(item->unknown);
	bw->Write(item->amount);
	bw->Write(item->probability);
	bw->Write(item->expiration);
	bw->Write(item->unkown_01);
}

void WriteRewardItem(int version, BinaryWriter^ bw, RewardItem^ item)
{
	bw->Write(item->id);
	bw->Write(item->unknown);
	bw->Write(item->amount);
	bw->Write(item->probability);
	bw->Write(item->flg);
	bw->Write(item->expiration);
	bw->Write(item->unkown_01);
	bw->Write(item->unkown_02);
	bw->Write(item->RefLevel);
	bw->Write(item->unkown_03);
}

void WriteDynamics(int version, BinaryWriter^ bw, Dynamics^ dynamic)
{
	bw->Write(dynamic->id);
	bw->Write(dynamic->unknown_1);
	bw->Write(dynamic->unknown_2);
	bw->Write(dynamic->unknown_3);
	bw->Write(dynamic->unknown_4);
	bw->Write(dynamic->unknown_5);
	WriteLocation(version,bw,dynamic->point);
	bw->Write(dynamic->times);
}

void WriteItemGroup(int version, BinaryWriter^ bw, ItemGroup^ item_group)
{
	bw->Write(item_group->type);
	bw->Write(item_group->items_count);
	for(int n=0; n<item_group->items->Length; n++)
	{
		WriteRewardItem(version, bw, item_group->items[n]);
	}
}

void WriteTeamMembers(int version, BinaryWriter^ bw, TeamMembers^ team_member_group)
{
	bw->Write(team_member_group->level_min);
	bw->Write(team_member_group->level_max);
	bw->Write(team_member_group->race);
	bw->Write(team_member_group->job);
	bw->Write(team_member_group->gender);
	bw->Write(team_member_group->Unknown_02);
	bw->Write(team_member_group->Unknown_01);
	bw->Write(team_member_group->amount_min);
	bw->Write(team_member_group->amount_max);
	bw->Write(team_member_group->quest);
}


void WriteChase(int version, BinaryWriter^ bw, Chase^ chase)
{
	bw->Write(chase->id_monster);
	bw->Write(chase->amount_monster);
	bw->Write(chase->id_drop);
	bw->Write(chase->amount_drop);
	bw->Write(chase->unknown_1);
	bw->Write(chase->probability);
	bw->Write(chase->unknown_2);
}


void WriteSpan(int version, BinaryWriter^ bw, Span^ location_span)
{
	bw->Write(location_span->east);
	bw->Write(location_span->bottom);
	bw->Write(location_span->south);
	bw->Write(location_span->west);
	bw->Write(location_span->top);
	bw->Write(location_span->north);
}

void WriteReward(int version, BinaryWriter^ bw, Reward^ reward)
{
	bw->Write(reward->coins);
	bw->Write(reward->experience);
	bw->Write(reward->ExpLv);
	bw->Write(reward->RCLv);
	bw->Write(reward->APLv);
	bw->Write(reward->new_quest);
	bw->Write(reward->unknow_01);
	bw->Write(reward->unknow_02);
	bw->Write(reward->unknow_03);
	bw->Write(reward->unknow_04);
	bw->Write(reward->unknow_05);
	bw->Write(reward->unknow_06);
	bw->Write(reward->unknow_07);
	bw->Write(reward->unknow_08);
	bw->Write(reward->unknow_09);
	bw->Write(reward->unknow_10);
	if(version>=165){
		bw->Write(reward->unkn_1380_1);
	}
	bw->Write(reward->group_integral);
	bw->Write(reward->unknow_12);
	bw->Write(reward->unknow_12_1);
	bw->Write(reward->unkn_1330_6);
	bw->Write(reward->fation_gongxian);
	bw->Write(reward->family_gongxian);
	bw->Write(reward->unknow_13);
	bw->Write(reward->title);
	bw->Write(reward->removePK);
	bw->Write(reward->clearPK);
	bw->Write(reward->Divorce);
	WriteSW(version, bw, reward->sw);
	bw->Write(reward->unknow_20);
	bw->Write(reward->unknow_21);
	bw->Write(reward->unknow_22);
	bw->Write(reward->unknow_23);
	bw->Write(reward->unknow_31);
	bw->Write(reward->unknow_25);
	bw->Write(reward->unknow_26);
	bw->Write(reward->unknow_27);
	bw->Write(reward->unknow_28);
	bw->Write(reward->unknow_29);
	bw->Write(reward->Cangku);
	bw->Write(reward->unknow_30);
	bw->Write(reward->Beibao);
	bw->Write(reward->CWBeibao);
	bw->Write(reward->CWlan);
	bw->Write(reward->ZuoqiBeibao);
	bw->Write(reward->unknow_31_1);
	bw->Write(reward->ShengChan_UP);
	bw->Write(reward->unknow_32);
	bw->Write(reward->jobtmp_up);
	bw->Write(reward->unknow_33);
	bw->Write(reward->SetJob);
	WriteLocation(version, bw, reward->teleport);
	bw->Write(reward->ai_trigger);
	bw->Write(reward->trigger_on);
	bw->Write(reward->unknow_35);
	bw->Write(reward->unknow_36);
	bw->Write(reward->ShuLianDu);
	bw->Write(reward->SkillsLV);
	bw->Write(reward->family_skill_id);
	bw->Write(reward->family_lingqi);
	bw->Write(reward->unkn_1330_7);
	bw->Write(reward->unkn_1330_8);
	bw->Write(reward->unknow_40);
	bw->Write(reward->fation_lingqi);
	bw->Write(reward->Notice);
	bw->Write(reward->Notice_Channel);
	bw->Write(reward->removetasks);
	bw->Write(reward->DoubleTimes);
	bw->Write(reward->item_groups_count);
	bw->Write(reward->unknow_42);
	bw->Write(reward->unknow_43);
	bw->Write(reward->ChuShi);
	bw->Write(reward->PanShi);
	bw->Write(reward->Shitu_01);
	bw->Write(reward->Shitu_02);
	bw->Write(reward->is_special_reward);
	bw->Write(reward->special_flg);
	bw->Write(reward->unknow_46);
	bw->Write(reward->Camp_on);
	bw->Write(reward->Camp);
	bw->Write(reward->skillcount);
	bw->Write(reward->skill_on);
	bw->Write(reward->skill_Tianshu_on);
	bw->Write(reward->is_exp_formula);
	bw->Write(reward->exp_formula_len);
	bw->Write(reward->unknow_48);
	for(int i=0;i<reward->value_set->Length;i++){
		WriteValueSet(version, bw, reward->value_set[i]);
	}
	bw->Write(reward->unknow_49);
	bw->Write(reward->Bianshen);
	bw->Write(reward->BianshenTime);
	bw->Write(reward->unknow_50);
	bw->Write(reward->unknow_51);
	bw->Write(reward->unknow_52);
	bw->Write(reward->YuanShen);
	bw->Write(reward->YuanShenlv);
	bw->Write(reward->OpenYuanYing);
	bw->Write(reward->HongLiIngot);
	if(version>=165){
		bw->Write(reward->unkn_1380_2);
		bw->Write(reward->first_time_count);
	}
	bw->Write(reward->unknown_53);
	bw->Write(reward->unknown_55);
	bw->Write(reward->unknown_56);
	bw->Write(reward->unknown_n1);
	bw->Write(reward->unknown_n2);
	bw->Write(reward->unknown_n3);
	bw->Write(reward->unknown_n4);
	bw->Write(reward->nval_1);
	bw->Write(reward->is_nval);
	bw->Write(reward->nval_2);
	bw->Write(reward->nval_len_1);
	bw->Write(reward->nval_3);
	bw->Write(reward->nval_len_2);
	bw->Write(reward->nval_4);
	bw->Write(reward->unkn_1330_10);
	bw->Write(reward->unkn_1330_9);
	if (reward->unknown_55 <= 0)
    {
		for(int i=0; i<reward->item_groups->Length; i++)
		{
			 WriteItemGroup(version, bw, reward->item_groups[i]);
		}
		for(int i=0; i<reward->Valitems->Length; i++)
		{
			WriteValueItem(version, bw, reward->Valitems[i]);
		}
		if (reward->is_nval)
        {
			bw->Write(reward->nval_5);
			bw->Write(reward->nval_6);
			bw->Write(reward->noticetext_len);
			bw->Write(reward->noticetext);
		}
		if ((reward->is_exp_formula > 0 || reward->exp_formula_len > 0)&&!reward->Shitu_01)
        {
			bw->Write(reward->exp_val->name);
			bw->Write(reward->exp_val->count);
			for(int i=0; i<reward->exp_val->val->Length; i++)
			{
				WriteExpValItems(version, bw, reward->exp_val->val[i]);
			}
		}
		for(int i=0; i<reward->first_time_reward->Length; i++)
		{
			 WriteItemGroup(version, bw, reward->first_time_reward[i]);
		}
	}
	else
    {
		if (reward->is_nval)
		{
			bw->Write(reward->nval_5);
			bw->Write(reward->nval_6);
		}
		for(int i=0; i<reward->item_groups->Length; i++)
		{
			 WriteItemGroup(version, bw, reward->item_groups[i]);
		}
		bw->Write(reward->noticetext_len);
		bw->Write(reward->noticetext);
	}
	if (reward->unknown_55 <= 0 && !reward->is_nval && (!reward->is_special_reward || (reward->special_flg <= 0)))
    {
		bw->Write(reward->unknown_54);
    }
}

void Task::Save(int version, BinaryWriter^ bw)
{

// ################# GENERAL #############################

	bw->Write(id);
	bw->Write(name);
	bw->Write(author_mode);
	bw->Write(UNKNOWN_01);
	bw->Write(type);
	bw->Write(time_limit);
	bw->Write(UNKNOWN_02);
	bw->Write(date_spans_count);
	bw->Write(UNKNOWN_157);
	bw->Write(xunhanType);
	bw->Write(cooldown);
	bw->Write(activate_first_subquest);
	bw->Write(activate_random_subquest);
	bw->Write(activate_next_subquest);
	bw->Write(on_give_up_parent_fails);
	bw->Write(on_success_parent_success);
	bw->Write(can_give_up);
	bw->Write(repeatable);
	bw->Write(repeatable_after_failure);
	bw->Write(fail_on_death);
	bw->Write(on_fail_parent_fail);
	bw->Write(UNKNOWN_10);
	bw->Write(player_limit);
	bw->Write(reset_type);
	bw->Write(reset_cycle);
	// trigger location
	bw->Write(trigger_locations->has_location);
	bw->Write(trigger_locations->map_id);
	WriteSpan(version, bw, trigger_locations->spans[0]);
	bw->Write(has_instant_teleport);
	WriteLocation(version, bw, instant_teleport_location);
	bw->Write(ai_trigger);
	bw->Write(has_trigger_on);
	bw->Write(UNKNOWN_ZEROS);
	bw->Write(UNKNOWN_18);
	bw->Write(UNKNOWN_19);
	bw->Write(autotask_unknown);
	bw->Write(UNKNOWN_20);
	bw->Write(cleartask_unknown);
	bw->Write(mark_available_icon);
	bw->Write(mark_available_point);
	bw->Write(quest_npc);
	bw->Write(reward_npc);
	bw->Write(UNKNOWN_17);
	bw->Write(UNKNOWN_21);
	bw->Write(UNKNOWN_22);
	bw->Write(component);
	bw->Write(classify);
	if(version>=165){
		bw->Write(unkn_1380_2);
	}
	bw->Write(UNKNOWN_23);
	bw->Write(clan_task);
	bw->Write(player_limit_on);
	bw->Write(all_zone);
	bw->Write(player_limit_num);
	bw->Write(UNKNOWN_112_1);
	bw->Write(all_zone_num);
	bw->Write(unkpenalty);
	bw->Write(playerpenalty);
	bw->Write(allkillnum);
	bw->Write(bool1380);
	bw->Write(UNKNOWN_113);
	bw->Write(haslimit);
	bw->Write(unkn_1330_11);
	bw->Write(fail_locations->has_location);
	bw->Write(fail_locations->map_id);
	WriteSpan(version, bw, fail_locations->spans[0]);
	bw->Write(GlobalVal1);
	WriteValueSet(version, bw, global_value_set1);
	WriteValueSet(version, bw, global_value_set2);
	WriteValueSet(version, bw, global_value_set3);
	bw->Write(GlobalVal2);
	bw->Write(val_unknown_01);
	bw->Write(val_unknown_02);
	bw->Write(val_unknown_03);
	bw->Write(new_type);
	bw->Write(new_type2);
	bw->Write(unkn_1330_1);
	bw->Write(unkn_1330_2);
	bw->Write(level_max);
	bw->Write(level_min);
	bw->Write(has_show_level);
	bw->Write(bloodbound_min);
	bw->Write(bloodbound_max);
	bw->Write(required_items_count);
	bw->Write(required_items_unknown);
	bw->Write(UNKNOWN_27);
	bw->Write(items_unknown_4);
	bw->Write(dynamics_count);
	bw->Write(items_unknown_6);
	bw->Write(items_unknown_5);
	bw->Write(items_unknown_7);
	bw->Write(given_items_count);
	bw->Write(given_items_needBags);
	bw->Write(items_unknown_9);
	bw->Write(items_unknown_10);
	bw->Write(items_unknown_11);
	bw->Write(required_reputation);
	bw->Write(instant_pay_coins);
	bw->Write(required_items_bags);
	bw->Write(required_need_next);
	bw->Write(has_next);
	bw->Write(clan_required);
	bw->Write(required_need_contribution);
	bw->Write(has_contribution);
	bw->Write(required_need_working);
	bw->Write(has_working);
	bw->Write(required_need_xianji);
	bw->Write(has_xianji);
	bw->Write(required_need_gender);
	bw->Write(has_gender);
	bw->Write(shengwang_guidao);
	bw->Write(shengwang_qingyun);
	bw->Write(shengwang_tianyin);
	bw->Write(shengwang_guiwang);
	bw->Write(shengwang_hehuan);
	bw->Write(shengwang_zhongyi);
	bw->Write(shengwang_qingyuan);
	bw->Write(shengwang_wencai);
	bw->Write(shengwang_shide);
	bw->Write(shengwang_yuliu_01);
	bw->Write(shengwang_yuliu_02);
	bw->Write(shengwang_yuliu_03);
	bw->Write(shengwang_yuliu_04);
	bw->Write(shengwang_jiuli);
	bw->Write(shengwang_lieshan);
	bw->Write(shengwang_chenhuang);
	bw->Write(shengwang_taihao);
	bw->Write(shengwang_huaiguang);
	bw->Write(shengwang_tianhua);
	bw->Write(shengwang_fenxiang);
	bw->Write(shengwang_unknown_48);
	bw->Write(has_shengwang);
	bw->Write(front_tasks_count);
	for(int i=0; i<front_tasks->Length; i++)
	{
		bw->Write(front_tasks[i]);
	}
	bw->Write(has_show_tasks);
	bw->Write(front_tasks_num_count);
	for(int i=0; i<front_tasks_a->Length; i++)
	{
		bw->Write(front_tasks_a[i]);
		bw->Write(front_tasks_num[i]);
	}
	bw->Write(front_tasks_yaoqiu);
	bw->Write(front_tasks_renwu);
	bw->Write(front_tasks_zhuanchonglv);
	bw->Write(front_tasks_showbangpai);
	bw->Write(UNKNOWN_114);
	bw->Write(front_tasks_showgender);
	bw->Write(front_tasks_yaoqiubanpai);
	bw->Write(front_tasks_xingbie);
	bw->Write(how_unknown01);
	bw->Write(job_nums);
	for(int i=0; i<jobs->Length; i++)
	{
		bw->Write(jobs[i]);
	}
	bw->Write(required_be_married);
	bw->Write(front_tasks_marry);
	bw->Write(UNKNOWN_115);
	bw->Write(mutex_tasks_num);
	for(int i=0; i<mutexs->Length; i++)
	{
		bw->Write(mutexs[i]);
	}
	bw->Write(UNKNOWN_116);
	bw->Write(UNKNOWN_31);
	bw->Write(pk_min);
	bw->Write(pk_max);
	bw->Write(required_be_gm);
	bw->Write(Team_asks);
	bw->Write(Team_share_again);
	bw->Write(Team_UNKNUWN_01);
	bw->Write(Team_share_now);
	bw->Write(UNKNOWN_118);
	bw->Write(Team_check_mem);
	bw->Write(Team_duizhang_shibai);
	bw->Write(Team_quan_shibai);
	bw->Write(UNKNOWN_119);
	bw->Write(Team_leave_shibai);
	bw->Write(UNKNOWN_120);
	bw->Write(required_team_member_groups_count);
	bw->Write(UNKNOWN_121);
	bw->Write(UNKNOWN_122);
	bw->Write(UNKNOWN_123);
	bw->Write(Shitu_yaoqiu);
	bw->Write(Shitu_shitu);
	bw->Write(Shitu_chushi);
	bw->Write(Shitu_pantaohui);
	bw->Write(Shitu_UNKNOWN_01);
	bw->Write(Shitu_UNKNOWN_02);
	bw->Write(Shitu_lv);
	bw->Write(Shitu_tasks);
	bw->Write(UNKNOWN_124);
	bw->Write(Jiazu_jiazu_tasks);
	bw->Write(Jiazu_zuzhang_tasks);
	bw->Write(Jiazu_jineng_lv_min);
	bw->Write(Jiazu_jineng_lv_max);
	bw->Write(Jiazu_jineng_shulian_min);
	bw->Write(Jiazu_jineng_shulian_max);
	bw->Write(Jiazu_jineng_id);
	bw->Write(Jiazu_map_id);
	bw->Write(Jiazu_tiaozhan_min);
	bw->Write(Jiazu_tiaozhan_max);
	bw->Write(UNKNOWN_125);
	bw->Write(BangLing_kouchu);
	bw->Write(BangLing_shuliang);
	bw->Write(UNKNOWN_126);
	bw->Write(front_tasks_feisheng_01);
	bw->Write(front_tasks_feisheng_02);
	bw->Write(front_tasks_feisheng);
	bw->Write(UNKNOWN_127);
	bw->Write(zhenying);
	bw->Write(Ingot_min);
	bw->Write(Ingot_max);
	bw->Write(unkn_1330_3);
	bw->Write(unkn_1330_4);
	if(version>=165){
		bw->Write(unkn_1380_1);
	}
	bw->Write(is_val);
	bw->Write(Nval_1);
	bw->Write(Nval_1_len);
	bw->Write(Nval_2);
	bw->Write(Nval_2_len);
	bw->Write(Nval_3);
	bw->Write(Nval_3_len);
	bw->Write(Nval_4);
	bw->Write(Nval_4_len);
	bw->Write(UNKNOWN_128);
	bw->Write(UNKNOWN_Soul_01);
	bw->Write(Soul_min);
	bw->Write(Soul_max);
	bw->Write(Soul_min_on);
	bw->Write(Soul_max_on);
	if(version>=165){
		bw->Write(unk1380int1);
		bw->Write(unk1380int2);
		bw->Write(unk1380int3);
	}
	bw->Write(Unknown_1628);
	bw->Write(JinXingFangShi);
	bw->Write(WanChengLeiXing);
	bw->Write(required_chases_count);
	bw->Write(UNKNOWN_129);
	bw->Write(required_get_items_count);
	bw->Write(UNKNOWN_234556);
	bw->Write(HuaFeiJinQian);
	bw->Write(SafeNPC);
	bw->Write(SafeNpcTime);
	if(version>=165){
		bw->Write(UNKNOWN_130a);
		bw->Write(UNKNOWN_130b);
	}
	bw->Write(UNKNOWN_130);
	bw->Write(reach_locations->has_location);
	WriteSpan(version, bw, reach_locations->spans[0]);
	bw->Write(UNKNOWN_131);
	bw->Write(reach_locations->map_id);
	bw->Write(required_wait_time);
	bw->Write(UNKNOWN_132);
	bw->Write(Title_count);
	for(int i=0; i<TitleS->Length; i++)
	{
		bw->Write(TitleS[i]);
	}
	bw->Write(needlv);
	bw->Write(UNKNOWN_133);
	bw->Write(is_val_1);
	bw->Write(sNval_1);
	bw->Write(sNval_1_len);
	bw->Write(sNval_2);
	bw->Write(sNval_2_len);
	bw->Write(sNval_3);
	bw->Write(sNval_3_len);
	bw->Write(sNval_4);
	bw->Write(sNval_4_len);
	bw->Write(sNval_5);
	bw->Write(reward_type);
	bw->Write(reward_type2);
	bw->Write(UNKNOWN_134);
	bw->Write(parent_quest);
	bw->Write(previous_quest);
	bw->Write(next_quest);
	bw->Write(sub_quest_first);
	if(author_mode)
	{
		bw->Write(author_text);
	}
	if(version>=165 && UNKNOWN_130b > 0){
		bw->Write(NEW_FLY_TYPE1380);
	}
	for(int i=0; i<date_spans->Length; i++)
	{
		WriteDateSpan(version, bw, date_spans[i]);
	}
	for(int i=0; i<tops->Length; i++)
	{
		bw->Write(tops[i]);
	}
	if(global_value_set1->val_count>0)
	{
		for(int i=0; i<global_value_1_items->Length; i++)
		{
			WriteValueItem(version, bw, global_value_1_items[i]);
		}
	}
	for(int i=0; i<required_items->Length; i++)
	{
		WriteItem(version, bw, required_items[i]);
	}
	for(int i=0; i<given_items->Length; i++)
	{
		WriteItem(version, bw, given_items[i]);
	}
	for(int i=0; i<dynamics->Length; i++)
	{
		WriteDynamics(version, bw, dynamics[i]);
	}
	if (is_val)
    {
		bw->Write(Nval_unknown_01);
		bw->Write(Nval_unknown_02);
		bw->Write(Nval_unknown_03);
		bw->Write(Nval_unknown_04);
	}
	if (is_val_1)
    {
		bw->Write(Nval_unknown_05);
		bw->Write(Nval_unknown_06);
		bw->Write(Nval_unknown_07);
		bw->Write(Nval_unknown_08);
	}
	for(int i=0; i<required_team_member_groups->Length; i++)
	{
		WriteTeamMembers(version, bw, required_team_member_groups[i]);
	}
	for(int i=0; i<required_chases->Length; i++)
	{
		WriteChase(version, bw, required_chases[i]);
	}
	for(int i=0; i<required_get_items->Length; i++)
	{
		WriteItem(version, bw, required_get_items[i]);
	}
// ################# SUCCESS REWARDS #############################

	WriteReward(version, bw, reward_success);
	if(reward_success->is_special_reward)
    {
		if (reward_success->special_flg == 1)
        {
			WriteReward(version, bw, reward_team_member);
			bw->Write(UNKNOWN_7);
        }
        if (reward_success->special_flg == 2)
        {
			WriteReward(version, bw, reward_team_leader);
			bw->Write(UNKNOWN_7);
        }
        if (reward_success->special_flg == 4)
        {
			WriteReward(version, bw, reward_teacher);
			if(reward_success->Shitu_01&&reward_success->exp_formula_len>0){
				bw->Write(reward_teacher->exp_val->name);
				bw->Write(reward_teacher->exp_val->count);
				for(int i=0; i<reward_teacher->exp_val->val->Length; i++)
				{
					WriteExpValItems(version, bw, reward_teacher->exp_val->val[i]);
				}
			}
			bw->Write(UNKNOWN_7);
        }
    }

// ################# FAILED REWARDS #############################

	WriteReward(version, bw, reward_failed);
	bw->Write(unknown_202);
	if (reward_type != 0)
    {
		if (reward_type == 1)
		{
			bw->Write(UNKNOWN_end);
			bw->Write(reward_items_count_2 );
			bw->Write(reward_items_id_2);
			for(int i=0; i<reward_items_group_2->Length; i++)
			{
				bw->Write(reward_items_group_2[i]);
			}
			for(int i=0; i<rewards_items_2->Length; i++)
			{
				WriteReward(version, bw, rewards_items_2[i]);
			}
			bw->Write(UNKNOWN_end2);
		}
	}
	else
	{
		if (reward_type2 != 3)
        {
			bw->Write(UNKNOWN_end);
        }
		else
        {
			bw->Write(UNKNOWN_end);
			bw->Write(reward_items_count_2);
			bw->Write(reward_items_id);
			//bw->Write(reward_items_id_2);
			for(int i=0; i<reward_items_group_2->Length; i++)
			{
				bw->Write(reward_items_group_2[i]);
			}
			for(int i=0; i<rewards_items_2->Length; i++)
			{
				WriteReward(version, bw, rewards_items_2[i]);
			}
			bw->Write(UNKNOWN_end2);
		}
		goto Label_1451; 
	}
	if (reward_type == 2)
	{
		bw->Write(rewards_timed_count);
		for(int i=0; i<rewards_timed_factors->Length; i++)
		{
			bw->Write(rewards_timed_factors[i]);
		}
		for(int i=0; i<rewards_timed->Length; i++)
		{
			WriteReward(version, bw, rewards_timed[i]);
		}
		bw->Write(UNKNOWN_end2);
	}
	if (reward_type == 3)
	{
		bw->Write(reward_items_count);
		bw->Write(reward_items_id);
		for(int i=0; i<reward_items_group->Length; i++)
		{
			bw->Write(reward_items_group[i]);
		}
		for(int i=0; i<rewards_items_new->Length; i++)
		{
			WriteReward(version, bw, rewards_items_new[i]);
		}
		bw->Write(UNKNOWN_end2);
	}
	if (reward_type == 4)
	{
		bw->Write(UNKNOWN_end);
		bw->Write(reward_num_count);
		for(int i=0; i<reward_num->Length; i++)
		{
			bw->Write(reward_num[i]);
		}
		for(int i=0; i<rewards_num->Length; i++)
		{
			WriteReward(version, bw, rewards_num[i]);
		}
		bw->Write(UNKNOWN_end2);
	}
	Label_1451:
// ################# CONVERSATION #############################

	bw->Write(conversation->prompt_character_count);
	bw->Write(conversation->prompt_text);
	bw->Write(conversation->agern_character_count);
	bw->Write(conversation->agern_text);
	bw->Write(conversation->seperator);
	bw->Write(conversation->general_character_count);
	bw->Write(conversation->general_text);
	bw->Write(conversation->xuanfu_character_count);
	bw->Write(conversation->xuanfu_text);
	bw->Write(conversation->new_character_count);
	bw->Write(conversation->new_text);
	for(int d=0; d<conversation->dialogs->Length; d++)
	{
		bw->Write(conversation->dialogs[d]->unknown);
		bw->Write(conversation->dialogs[d]->dialog_text);
		bw->Write(conversation->dialogs[d]->question_count);
		for(int q=0; q<conversation->dialogs[d]->questions->Length; q++)
		{
			bw->Write(conversation->dialogs[d]->questions[q]->question_id);
			bw->Write(conversation->dialogs[d]->questions[q]->previous_question);
			bw->Write(conversation->dialogs[d]->questions[q]->question_character_count);
			bw->Write(conversation->dialogs[d]->questions[q]->question_text);
			bw->Write(conversation->dialogs[d]->questions[q]->answer_count);
			for(int a=0; a<conversation->dialogs[d]->questions[q]->answer_count; a++)
			{
				bw->Write(conversation->dialogs[d]->questions[q]->answers[a]->question_link);
				bw->Write(conversation->dialogs[d]->questions[q]->answers[a]->answer_text);
				bw->Write(conversation->dialogs[d]->questions[q]->answers[a]->task_link);
			}
		}
	}

// ################# SUB TASKS #############################

	bw->Write(sub_quest_count);

	for(int i=0; i<sub_quest_count; i++)
	{
		sub_quests[i]->Save(version, bw);
	}
}